package plugins.ylemontag.mathoperations;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTextField;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component to edit formatted content generated from string
 */
public abstract class FormattedTextField<T> extends JTextField
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener interface
	 */
	public static interface ValueListener<T>
	{
		/**
		 * Method triggered when the value in the component changes
		 */
		public void valueChanged(T newValue);
	}
	
	private List<ValueListener<T>> _listeners   ;
	private boolean                _isTextEmpty ;
	private T                      _currentValue;
	
	/**
	 * Constructor
	 */
	protected FormattedTextField()
	{
		super();
		_listeners = new LinkedList<ValueListener<T>>();
		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onComponentValueChanged(getText());
			}
		});
		addFocusListener(new FocusAdapter()
		{
			@Override
			public void focusLost(FocusEvent e)
			{
				onComponentValueChanged(getText());
			}

			@Override
			public void focusGained(FocusEvent e)
			{
				if(_isTextEmpty) {
					setText("");
					setForeground(Color.BLACK);
				}
			}
		});
	}
	
	/**
	 * Add a new listener
	 * @param l Listener
	 */
	public void addValueListener(ValueListener<T> l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 * @param l Listener
	 */
	public void removeValueListener(ValueListener<T> l)
	{
		_listeners.remove(l);
	}
	
	/**
	 * @return Retrieve the object
	 */
	public T getValue()
	{
		return _currentValue;
	}
	
	/**
	 * Set the object displayed in the component
	 * @param value Generic Object
	 */
	public void setValue(T value)
	{
		T oldValue = _currentValue;
		_currentValue = value;
		updateComponentText(objectToString(_currentValue));
		if(areIdenticObjectsOrNull(_currentValue, oldValue)) {
			return;
		}
		fireValueListeners();
	}
	
	/**
	 * Must be called the constructor of derived classes
	 */
	protected void initializeComponent()
	{
		_currentValue = stringToObject("");
		updateComponentText("");
	}
	
	@Override
	public void setEnabled(boolean enabled)
	{
		setBackground(enabled ? Color.WHITE : Color.GRAY);
		super.setEnabled(enabled);
	}
	
	/**
	 * @return String to show in the text field when it is empty and it does not have the focus
	 */
	protected abstract String makeHelpText();
	
	/**
	 * @return Parse the given string to generate a new T object
	 * @param text String
	 * Remarks - If the parsing fails, the function must return null.
	 */
	protected abstract T stringToObject(String text);
	
	/**
	 * @return Return the string representation for the given object
	 * @param object Generic Object
	 * Remarks - The argument may be set to null.
	 */
	protected abstract String objectToString(T object);
	
	/**
	 * @return Return true if both objects are equals
	 * @param obj1 Generic Object
	 * @param obj2 Generic Object
	 * Remarks - The arguments are never null
	 */
	protected abstract boolean areIdenticObjects(T obj1, T obj2);
	
	/**
	 * @return Return true if both objects are equals, or if both arguments are null
	 * @param obj1 Generic Object
	 * @param obj2 Generic Object
	 */
	private boolean areIdenticObjectsOrNull(T obj1, T obj2)
	{
		if(obj1==null) {
			return obj2==null;
		}
		else if(obj2==null) {
			return false;
		}
		else {
			return areIdenticObjects(obj1, obj2);
		}
	}
	
	/**
	 * Action performed when the component lost the focus or the user hit the <Enter> key
	 * @param newText String
	 */
	private void onComponentValueChanged(String newText)
	{
		T oldValue = _currentValue;
		_currentValue = stringToObject(newText);
		updateComponentText(_currentValue==null ? newText : objectToString(_currentValue));
		if(areIdenticObjectsOrNull(_currentValue, oldValue)) {
			return;
		}
		fireValueListeners();
	}
	
	/**
	 * Set the text in the text field
	 * @param text String
	 */
	private void updateComponentText(String text)
	{
		if(text.isEmpty()) {
			_isTextEmpty = true;
			if(hasFocus()) {
				setText("");
				setForeground(Color.BLACK);
			}
			else {
				setText(makeHelpText());
				setForeground(Color.GRAY);
			}
		}
		else {
			_isTextEmpty = false;
			setText(text);
			setForeground(_currentValue==null ? Color.RED : Color.BLACK);
		}
	}
	
	/**
	 * Fire the value listeners
	 */
	private void fireValueListeners()
	{
		for(ValueListener<T> l : _listeners) {
			l.valueChanged(_currentValue);
		}
	}
}
