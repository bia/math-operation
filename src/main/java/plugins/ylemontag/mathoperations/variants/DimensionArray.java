package plugins.ylemontag.mathoperations.variants;

import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Class that characterize the dimension of an array
 * 
 * The dimension of an array is fully defined by its length.
 */
public class DimensionArray extends Variant.Dimension
{
	private long _length;
	
	public DimensionArray(long length)
	{
		super(Variant.DimensionType.ARRAY);
		_length = length;
	}
	
	public long getLength()
	{
		return _length;
	}
	
	@Override
	public boolean equals(Variant.Dimension dimension)
	{
		if(!(dimension instanceof DimensionArray)) {
			return false;
		}
		DimensionArray dim = (DimensionArray)dimension;
		return _length==dim._length;
	}
	
	@Override
	public String getRepresentation()
	{
		return "array of length " + _length;
	}
	
	@Override
	public long getFlatSize()
	{
		return _length;
	}
	
	@Override
	public int getGranularity()
	{
		return 1;
	}
	
	@Override
	public Variant allocateNewVariant()
	{
		return new VariantArray(_length);
	}
}
