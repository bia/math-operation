package plugins.ylemontag.mathoperations.variants;

import icy.util.StringUtil;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a scalar value as a Variant object
 */
public class VariantScalar extends Variant
{	
	private double _value;
	
	/**
	 * Constructor (create)
	 */
	public VariantScalar()
	{
		this(0.0);
	}
	
	/**
	 * Constructor (wrap)
	 */
	public VariantScalar(double value)
	{
		super(Type.SCALAR);
		_value = value;
	}
	
	@Override
	public double getAsScalar()
	{
		return _value;
	}
	
	@Override
	public String getRepresentation(boolean addParenthesisIfFragile)
	{
		String retVal = StringUtil.toString(_value);
		if(addParenthesisIfFragile && _value<0) {
			retVal = "(" + retVal + ")";
		}
		return retVal;
	}
	
	@Override
	public Dimension getDimension()
	{
		return new DimensionScalar();
	}
	
	@Override
	public boolean isReadable()
	{
		return true;
	}
	
	@Override
	public boolean isWritable()
	{
		return true;
	}
	
	@Override
	protected ReadIterator implGetReadIterator()
	{
		return new ReadScalarIterator(_value);
	}
	
	@Override
	protected WriteIterator implGetWriteIterator()
	{
		return new WriteScalarIterator(this);
	}
	
	@Override
	protected void implBeginUpdate() {}
	
	@Override
	protected void implEndUpdate() {}
	
	
	/**
	 * Scalar iterator, read mode
	 */
	private static class ReadScalarIterator implements ReadIterator
	{
		double _value;
		
		public ReadScalarIterator(double value)
		{
			_value = value;
		}
		
		@Override
		public boolean valid()
		{
			return true;
		}

		@Override
		public void next() {}

		@Override
		public void startAt(long num) {}

		@Override
		public double get()
		{
			return _value;
		}
	}
	
	
	/**
	 * Scalar iterator, write mode
	 */
	private static class WriteScalarIterator implements WriteIterator
	{
		private VariantScalar _target;
		private boolean       _valid ;
		
		public WriteScalarIterator(VariantScalar target)
		{
			_target = target;
			_valid  = true;
		}
		
		@Override
		public boolean valid()
		{
			return _valid;
		}
		
		@Override
		public void next()
		{
			_valid = false;
		}
		
		@Override
		public void startAt(long num)
		{
			if(num>0) {
				_valid = false;
			}
		}
		
		@Override
		public void set(double value)
		{
			_target._value = value;
		}
	}
}
