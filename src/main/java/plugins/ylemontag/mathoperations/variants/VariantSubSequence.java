package plugins.ylemontag.mathoperations.variants;

import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import plugins.ylemontag.mathoperations.SubSequence;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a sub-sequence as a Variant object
 */
public class VariantSubSequence extends Variant
{
	private SubSequence _value;
	
	/**
	 * Constructor (wrap)
	 * @throws IllegalArgumentException If value==null
	 */
	public VariantSubSequence(SubSequence value)
	{
		super(Type.SUBSEQUENCE);
		if(value==null) {
			throw new IllegalArgumentException(
				"Cannot wrap a null sub-sequence in a Variant object."
			);
		}
		_value = value;
	}
	
	@Override
	public SubSequence getAsSubSequence()
	{
		return _value;
	}

	@Override
	public String getRepresentation(boolean addParenthesisIfFragile)
	{
		String name = _value.getSequence().getName();
		if(addParenthesisIfFragile && isFragileString(name)) {
			return "(" + name + ")" + _value.getSubRangeAsString();
		}
		else {
			return _value.toString();
		}
	}
	
	@Override
	public Dimension getDimension()
	{
		return new DimensionSequence(_value.getSizeX(), _value.getSizeY(), _value.getSizeZ(),
			_value.getSizeT(), _value.getSizeC());
	}
	
	@Override
	public boolean isReadable()
	{
		return _value.isValid();
	}

	@Override
	public boolean isWritable()
	{
		return false;
	}

	@Override
	protected ReadIterator implGetReadIterator()
	{
		if(_value.getSequence().getDataType_()==DataType.DOUBLE) {
			return new DoubleValuedSubSequenceIterator(_value);
		}
		else {
			return new ConvertOnFlySubSequenceIterator(_value);
		}
	}

	@Override
	protected WriteIterator implGetWriteIterator()
	{
		throw new RuntimeException("Unreachable code point");
	}

	@Override
	protected void implBeginUpdate()
	{
		throw new RuntimeException("Unreachable code point");
	}

	@Override
	protected void implEndUpdate()
	{
		throw new RuntimeException("Unreachable code point");
	}
	
	
	/**
	 * Base class for sub-sequence iterator in read mode
	 */
	private static abstract class ReadSubSequenceIterator extends SequenceIterators.Read
	{
		private final int[] _indexT   ;
		private final int[] _indexZ   ;
		private final int[] _indexC   ;
		protected Sequence  _rawSource;
		protected int       _actualT  ;
		protected int       _actualZ  ;
		protected int       _actualC  ;
		
		public ReadSubSequenceIterator(SubSequence source)
		{
			super(source.getSizeX(), source.getSizeY(), source.getSizeZ(), source.getSizeT(), source.getSizeC());
			_indexT    = source.getT();
			_indexZ    = source.getZ();
			_indexC    = source.getC();
			_rawSource = source.getSequence();
		}
		
		/**
		 * Must be called at the beginning of the refreshBuffer() method
		 */
		protected void refreshActualCoordinates()
		{
			_actualT = (_indexT==null ? _t : _indexT[_t]);
			_actualZ = (_indexZ==null ? _z : _indexZ[_z]);
			_actualC = (_indexC==null ? _c : _indexC[_c]);
		}
	}
	
	
	/**
	 * Double-valued sub-sequence iterator, read mode
	 */
	private static class DoubleValuedSubSequenceIterator extends ReadSubSequenceIterator
	{
		public DoubleValuedSubSequenceIterator(SubSequence source)
		{
			super(source);
		}
		
		@Override
		protected void refreshBuffer()
		{
			refreshActualCoordinates();
			_buffer = _rawSource.getDataXYAsDouble(_actualT, _actualZ, _actualC);
		}
	}
	
	
	/**
	 * Non-double-valued sub-sequence iterator, read mode
	 */
	private static class ConvertOnFlySubSequenceIterator extends ReadSubSequenceIterator
	{
		private boolean _isSigned ;
		
		public ConvertOnFlySubSequenceIterator(SubSequence source)
		{
			super(source);
			_isSigned = _rawSource.getDataType_().isSigned();
			_buffer   = new double[_sizeXY];
		}
		
		@Override
		protected void refreshBuffer()
		{
			refreshActualCoordinates();
			Array1DUtil.arrayToDoubleArray(_rawSource.getDataXY(_actualT, _actualZ, _actualC),
				_buffer, _isSigned);
		}
	}
}
