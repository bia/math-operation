package plugins.ylemontag.mathoperations.variants;

import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Collection of abstract classes that are used to defined sequence and sub-sequence iterators
 */
public class SequenceIterators
{
	/**
	 * Base class for sequence iterators
	 */
	public static abstract class Base implements Variant.Iterator
	{
		protected double[] _buffer;
		protected int _sizeXY;
		protected int _sizeC ;
		protected int _sizeZ ;
		protected int _sizeT ;
		protected int _xy;
		protected int _c ;
		protected int _z ;
		protected int _t ;
		
		protected Base(int sizeX, int sizeY, int sizeZ, int sizeT, int sizeC)
		{
			_sizeXY = sizeX*sizeY;
			_sizeC  = sizeC;
			_sizeZ  = sizeZ;
			_sizeT  = sizeT;
			_xy = 0;
			_c  = 0;
			_z  = 0;
			_t  = 0;
		}
		
		/**
		 * Refresh the buffer
		 */
		protected abstract void refreshBuffer();
		
		/**
		 * Invalidate the buffer (when the last sample in the sequence has been visited)
		 */
		protected void invalidateBuffer()
		{
			_buffer = null;
		}
		
		@Override
		public boolean valid()
		{
			return _buffer!=null;
		}
		
		@Override
		public void next()
		{
			++_xy;
			if(_xy>=_sizeXY) {
				_xy = 0;
				++_c;
				if(_c>=_sizeC) {
					_c = 0;
					++_z;
					if(_z>=_sizeZ) {
						_z = 0;
						++_t;
						if(_t>=_sizeT) {
							invalidateBuffer();
							return;
						}
					}
				}
				refreshBuffer();
			}
		}

		@Override
		public void startAt(long num)
		{
			// Special case for zero-size sequences
			if(_sizeXY==0 || _sizeZ==0 || _sizeT==0 || _sizeC==0) {
				invalidateBuffer();
				return;
			}
			
			int carry = 0;
			
			// Update XY
			if(num>0 || carry!=0)
			{
				_xy += (num % _sizeXY) + carry; // Here _xy should not change as it is
				num /= _sizeXY;                 // assumed that 'num' is a multiple of
				if(_xy>=_sizeXY) {              // the granularity, which is sizeXY in
					carry = 1;                    // case of sequences.
					_xy  -= _sizeXY;
				}
				else {
					carry = 0;
				}
				
				// Update C
				if(num>0 || carry!=0)
				{
					_c  += (num % _sizeC) + carry;
					num /= _sizeC;
					if(_c>=_sizeC) {
						carry = 1;
						_c   -= _sizeC;
					}
					else {
						carry = 0;
					}
					
					// Update Z
					if(num>0 || carry!=0)
					{
						_z  += (num % _sizeZ) + carry;
						num /= _sizeZ;
						if(_z>=_sizeZ) {
							carry = 1;
							_z   -= _sizeZ;
						}
						else {
							carry = 0;
						}
						
						// Update T
						if(num>0 || carry!=0)
						{
							_t  += (num % _sizeT) + carry;
							num /= _sizeT;
							if(_t>=_sizeT) {
								carry = 1;
								_t   -= _sizeT;
							}
							else {
								carry = 0;
							}
							
							// Set the cursor in its terminal state in case of "overflow"
							if(num>0 || carry!=0) {
								invalidateBuffer();
								return;
							}
						}
					}
				}
			}
			
			// Initialize the buffer
			refreshBuffer();
		}
	}
	
	
	/**
	 * Base class for read-mode iterators
	 */
	public static abstract class Read extends Base implements Variant.ReadIterator
	{
		protected Read(int sizeX, int sizeY, int sizeZ, int sizeT, int sizeC)
		{
			super(sizeX, sizeY, sizeZ, sizeT, sizeC);
		}
		
		@Override
		public double get()
		{
			return _buffer[_xy];
		}
	}
	
	
	/**
	 * Base class for write-mode iterators
	 */
	public static abstract class Write extends Base implements Variant.WriteIterator
	{
		protected Write(int sizeX, int sizeY, int sizeZ, int sizeT, int sizeC)
		{
			super(sizeX, sizeY, sizeZ, sizeT, sizeC);
		}
		
		@Override
		public void set(double value)
		{
			_buffer[_xy] = value;
		}
	}
}
