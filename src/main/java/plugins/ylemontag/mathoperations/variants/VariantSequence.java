package plugins.ylemontag.mathoperations.variants;

import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a sequence as a Variant object
 */
public class VariantSequence extends Variant
{
	private Sequence _value;
	private int      _sizeX;
	private int      _sizeY;
	private int      _sizeZ;
	private int      _sizeT;
	private int      _sizeC;
	private ImagePrecursor[] _precursors       ;
	private boolean          _dynamicAllocation;
	
	/**
	 * Constructor (create)
	 */
	public VariantSequence(int sizeX, int sizeY, int sizeZ, int sizeT, int sizeC)
	{
		super(Type.SEQUENCE);
		_value = new Sequence();
		_sizeX = sizeX;
		_sizeY = sizeY;
		_sizeZ = sizeZ;
		_sizeT = sizeT;
		_sizeC = sizeC;
		_dynamicAllocation = true;
	}
	
	/**
	 * Constructor (wrap)
	 * @throws IllegalArgumentException If value==null
	 */
	public VariantSequence(Sequence value)
	{
		super(Type.SEQUENCE);
		if(value==null) {
			throw new IllegalArgumentException(
				"Cannot wrap a null sequence in a Variant object."
			);
		}
		_value = value;
		_sizeX = _value.getSizeX();
		_sizeY = _value.getSizeY();
		_sizeZ = _value.getSizeZ();
		_sizeT = _value.getSizeT();
		_sizeC = _value.getSizeC();
		_dynamicAllocation = false;
	}
	
	@Override
	public Sequence getAsSequence()
	{
		return _value;
	}
	
	@Override
	public String getRepresentation(boolean addParenthesisIfFragile)
	{
		String retVal = _value.getName();
		if(addParenthesisIfFragile && isFragileString(retVal)) {
			retVal = "(" + retVal + ")";
		}
		return retVal;
	}
	
	@Override
	public Dimension getDimension()
	{
		return new DimensionSequence(_sizeX, _sizeY, _sizeZ, _sizeT, _sizeC);
	}
	
	@Override
	public boolean isReadable()
	{
		return !_dynamicAllocation;
	}
	
	@Override
	public boolean isWritable()
	{
		DataType datatype = _value.getDataType_();
		return datatype==DataType.DOUBLE || datatype==DataType.UNDEFINED;
	}
	
	@Override
	protected ReadIterator implGetReadIterator()
	{
		if(_value.getDataType_()==DataType.DOUBLE) {
			return new DoubleValuedSequenceIterator(_value);
		}
		else {
			return new ConvertOnFlySequenceIterator(_value);
		}
	}
	
	@Override
	protected WriteIterator implGetWriteIterator()
	{
		if(_dynamicAllocation) {
			return new DynamicSequenceIterator(this);
		}
		else {
			return new PreAllocatedSequenceIterator(this);
		}
	}
	
	@Override
	protected void implBeginUpdate()
	{
		_value.beginUpdate();
		if(_dynamicAllocation) {
			_value.removeAllImages();
			_precursors = new ImagePrecursor[_sizeZ*_sizeT];
			for(int k=0; k<_precursors.length; ++k) {
				_precursors[k] = new ImagePrecursor(this);
			}
		}
	}
	
	@Override
	protected void implEndUpdate()
	{
		_precursors = null;
		if(_dynamicAllocation
			&& _sizeX==_value.getSizeX()
			&& _sizeY==_value.getSizeY()
			&& _sizeZ==_value.getSizeZ()
			&& _sizeT==_value.getSizeT()
			&& _sizeC==_value.getSizeC()			
		) {
			_dynamicAllocation = false;
		}
		_value.endUpdate();
	}
	
	
	/**
	 * Double-valued sequence iterator, read mode
	 */
	private static class DoubleValuedSequenceIterator extends SequenceIterators.Read
	{
		private Sequence _source;
		
		public DoubleValuedSequenceIterator(Sequence source)
		{
			super(source.getSizeX(), source.getSizeY(), source.getSizeZ(), source.getSizeT(), source.getSizeC());
			_source = source;
		}
		
		@Override
		protected void refreshBuffer()
		{
			_buffer = _source.getDataXYAsDouble(_t, _z, _c);
		}
	}
	
	
	/**
	 * Non-double-valued sequence iterator, read mode
	 */
	private static class ConvertOnFlySequenceIterator extends SequenceIterators.Read
	{
		private Sequence _source  ;
		private boolean  _isSigned;
		
		public ConvertOnFlySequenceIterator(Sequence source)
		{
			super(source.getSizeX(), source.getSizeY(), source.getSizeZ(), source.getSizeT(), source.getSizeC());
			_source   = source;
			_isSigned = _source.getDataType_().isSigned();
			_buffer   = new double[_sizeXY];
		}
		
		@Override
		protected void refreshBuffer()
		{
			Array1DUtil.arrayToDoubleArray(_source.getDataXY(_t, _z, _c), _buffer, _isSigned);
		}
	}
	
	
	/**
	 * Pre-allocated sequences, write mode
	 */
	private static class PreAllocatedSequenceIterator extends SequenceIterators.Write
	{
		private VariantSequence _target;
		
		public PreAllocatedSequenceIterator(VariantSequence target)
		{
			super(target._sizeX, target._sizeY, target._sizeZ, target._sizeT, target._sizeC);
			_target = target;
		}
		
		@Override
		protected void refreshBuffer()
		{
			_buffer = _target.getAsSequence().getDataXYAsDouble(_t, _z, _c);
		}
	}
	
	
	/**
	 * Dynamically-allocated sequences, write mode
	 */
	private static class DynamicSequenceIterator extends SequenceIterators.Write
	{
		private VariantSequence _target   ;
		private ImagePrecursor  _precursor;
		private int             _previousT;
		private int             _previousZ;
		private int             _previousC;
		
		public DynamicSequenceIterator(VariantSequence target)
		{
			super(target._sizeX, target._sizeY, target._sizeZ, target._sizeT, target._sizeC);
			_target = target;
		}
		
		@Override
		protected void refreshBuffer()
		{
			// Save the previously-computed values
			saveCurrentBuffer();
			
			// Update the buffer
			_precursor = _target._precursors[_z + _sizeZ*_t];
			_previousT = _t;
			_previousZ = _z;
			_previousC = _c;
			_buffer    = new double[_sizeXY];
		}
		
		@Override
		protected void invalidateBuffer()
		{
			saveCurrentBuffer();
			super.invalidateBuffer();
		}
		
		/**
		 * Save the current buffer
		 */
		private void saveCurrentBuffer()
		{
			if(_precursor!=null) {
				_precursor.appendData(_previousT, _previousZ, _previousC, _buffer);
			}
		}
	}
	
	
	/**
	 * In case of dynamically allocated sequences, each image is build progressively,
	 * and appended to the sequence when the all the pixel-value buffers corresponding
	 * to all the channels are available.
	 * 
	 * @remarks The current implementation works in a multi-thread context,
	 *          assuming that the granularity is at least _sizeX*_sizeY.
	 */
	private static class ImagePrecursor
	{
		private VariantSequence _target;
		private Object[]        _data  ;
		private boolean         _done  ;
		
		/**
		 * Constructor
		 */
		public ImagePrecursor(VariantSequence target)
		{
			_target = target;
			_data   = new double[_target._sizeC][];
			_done   = false;
		}
		
		/**
		 * Set the pixel-value buffer corresponding to the given channel
		 * 
		 * If the results for all the channels are available, try to create the
		 * buffered image and to append it to the given sequence
		 */
		public void appendData(int t, int z, int c, double[] dataXY)
		{
			// Save the current channel
			_data[c] = dataXY;
			
			// Check that all the results for all channels are available
			for(Object o : _data) {
				if(o==null) {
					return;
				}
			}
			
			// Synchronization is needed here, as several threads may try to finalize
			// the current image at the same time
			synchronized (this)
			{
				// Nothing to do if the image has been finalized during the synchronization
				if(_done) {
					return;
				}
				
				// Create the buffered image, and update the sequence
				IcyBufferedImage image = new IcyBufferedImage(_target._sizeX, _target._sizeY, _data);
				synchronized (_target._value) {
					_target._value.setImage(t, z, image);
				}
				
				// Flag the precursor as finalized
				_done = true;
			}
		}
	}
}
