package plugins.ylemontag.mathoperations.variants;

import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Characterize the dimensions of a sequence
 */
public class DimensionSequence extends Variant.Dimension
{
	private int _sizeX;
	private int _sizeY;
	private int _sizeZ;
	private int _sizeT;
	private int _sizeC;
	
	public DimensionSequence(int sizeX, int sizeY, int sizeZ, int sizeT, int sizeC)
	{
		super(Variant.DimensionType.SEQUENCE);
		_sizeX = sizeX;
		_sizeY = sizeY;
		_sizeZ = sizeZ;
		_sizeT = sizeT;
		_sizeC = sizeC;
	}
	
	public int getSizeX() { return _sizeX; }
	public int getSizeY() { return _sizeY; }
	public int getSizeZ() { return _sizeZ; }
	public int getSizeT() { return _sizeT; }
	public int getSizeC() { return _sizeC; }
	
	@Override
	public boolean equals(Variant.Dimension dimension)
	{
		if(!(dimension instanceof DimensionSequence)) {
			return false;
		}
		DimensionSequence dim = (DimensionSequence)dimension;
		return
			_sizeX==dim._sizeX &&
			_sizeY==dim._sizeY &&
			_sizeZ==dim._sizeZ &&
			_sizeT==dim._sizeT &&
			_sizeC==dim._sizeC;
	}
	
	@Override
	public String getRepresentation()
	{
		return String.format("sequence of size (X,Y,Z,T,C)=(%d,%d,%d,%d,%d)",
			_sizeX, _sizeY, _sizeZ, _sizeT, _sizeC);
	}
	
	@Override
	public long getFlatSize()
	{
	    long result = _sizeX;
	    result *= _sizeY;
        result *= _sizeZ;
        result *= _sizeT;
        result *= _sizeC;
		return result;
	}
	
	@Override
	public int getGranularity()
	{
		return _sizeX*_sizeY;
	}
	
	@Override
	public Variant allocateNewVariant()
	{
		return new VariantSequence(_sizeX, _sizeY, _sizeZ, _sizeT, _sizeC);
	}
}
