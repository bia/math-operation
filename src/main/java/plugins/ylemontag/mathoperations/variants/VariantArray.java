package plugins.ylemontag.mathoperations.variants;

import icy.util.StringUtil;
import plugins.ylemontag.mathoperations.Functor.UnsupportedArguments;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a double array as a Variant object
 */
public class VariantArray extends Variant
{
	private double[] _value;
	
	/**
	 * Constructor (create)
	 */
	public VariantArray(long length)
	{
		super(Type.ARRAY);

		if (length >= Integer.MAX_VALUE)
		    throw new UnsupportedArguments("Data exceeding maximum size (" + length + " >=  Integer.MAX_VALUE) !");
		
		_value = new double[(int) length];
	}
	
	/**
	 * Constructor (wrap)
	 * @throws IllegalArgumentException If value==null
	 */
	public VariantArray(double[] value)
	{
		super(Type.ARRAY);
		if(value==null) {
			throw new IllegalArgumentException(
				"Cannot wrap a null double array in a Variant object."
			);
		}
		_value = value;
	}
	
	@Override
	public double[] getAsArray()
	{
		return _value;
	}
	
	@Override
	public String getRepresentation(boolean addParenthesisIfFragile)
	{
		String retVal = "[";
		boolean first = true;
		for(double v : _value) {
			if(first) {
				first = false;
			}
			else {
				retVal += " ";
			}
			retVal += StringUtil.toString(v);
		}
		retVal += "]";
		return retVal;
	}
	
	@Override
	public Dimension getDimension()
	{
		return new DimensionArray(_value.length);
	}
	
	@Override
	public boolean isReadable()
	{
		return true;
	}
	
	@Override
	public boolean isWritable()
	{
		return true;
	}
	
	@Override
	protected ReadIterator implGetReadIterator()
	{
		return new ReadArrayIterator(_value);
	}
	
	@Override
	protected WriteIterator implGetWriteIterator()
	{
		return new WriteArrayIterator(_value);
	}
	
	@Override
	protected void implBeginUpdate() {}
	
	@Override
	protected void implEndUpdate() {}
	
	
	/**
	 * Base class for array iterators
	 */
	private static abstract class ArrayIterator implements Iterator
	{
		protected double[] _buffer;
		protected int      _k     ;
		
		protected ArrayIterator(double[] buffer)
		{
			_buffer = buffer;
			_k      = 0;
		}
		
		@Override
		public boolean valid()
		{
			return _k<_buffer.length;
		}

		@Override
		public void next()
		{
			++_k;
		}

		@Override
		public void startAt(long num)
		{
		    if (_k + num >= Integer.MAX_VALUE)
		        throw new UnsupportedArguments("Data exceeding maximum size (" + (_k + num) + " >=  Integer.MAX_VALUE) !");

		    _k += num;
		}
	}
	
	
	/**
	 * Array iterator, read mode
	 */
	private static class ReadArrayIterator extends ArrayIterator implements ReadIterator
	{
		public ReadArrayIterator(double[] buffer)
		{
			super(buffer);
		}
		
		@Override
		public double get()
		{
			return _buffer[_k];
		}
	}
	
	
	/**
	 * Array iterator, write mode
	 */
	private static class WriteArrayIterator extends ArrayIterator implements WriteIterator
	{
		public WriteArrayIterator(double[] buffer)
		{
			super(buffer);
		}

		@Override
		public void set(double value)
		{
			_buffer[_k] = value;
		}
	}
}
