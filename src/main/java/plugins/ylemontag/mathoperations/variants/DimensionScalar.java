package plugins.ylemontag.mathoperations.variants;

import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Class that characterize the dimension of a scalar object
 * 
 * This class is quite trivial, but is needed to compel with the interface
 * define in the abstract class Variant.
 */
public class DimensionScalar extends Variant.Dimension
{
	public DimensionScalar()
	{
		super(Variant.DimensionType.SCALAR);
	}
	
	@Override
	public boolean equals(Variant.Dimension dimension)
	{
		return (dimension instanceof DimensionScalar);
	}
	
	@Override
	public String getRepresentation()
	{
		return "scalar";
	}
	
	@Override
	public long getFlatSize()
	{
		return 1;
	}
	
	@Override
	public int getGranularity()
	{
		return 1;
	}
	
	@Override
	public Variant allocateNewVariant()
	{
		return new VariantScalar();
	}
}
