package plugins.ylemontag.mathoperations;

import java.util.LinkedList;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Object used to control and interrupt long operations
 */
public class Controller
{
	/**
	 * Exception thrown when a computation have been interrupted
	 */
	public static class CanceledByUser extends Exception
	{
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * This interface is used to prob the progress of the jobs that are in
	 * charge of the computation. There is one JobProber per job.
	 */
	public interface JobProbe
	{
		/**
		 * Current step
		 */
		public long getCurrentStep();
		
		/**
		 * Number of steps
		 */
		public long getMaxStep();
	}
	
	private boolean              _cancelFlag;
	private LinkedList<JobProbe> _probes    ;
	
	/**
	 * Constructor
	 */
	public Controller()
	{
		_cancelFlag = false;
		_probes     = new LinkedList<JobProbe>();
	}
	
	/**
	 * Add a new job probe
	 */
	public void pushProbe(JobProbe probe)
	{
		synchronized (_probes)
		{
			_probes.addLast(probe);
		}
	}
	
	/**
	 * Remove all the job probes
	 */
	public void clearProbes()
	{
		synchronized (_probes)
		{
			_probes.clear();
		}
	}
	
	/**
	 * Return the current percentage of progress
	 */
	public double getProgress()
	{
		// Aggregate the current/max steps information associated to all the jobs 
		int currentStep = 0;
		int maxStep     = 0;
		synchronized (_probes)
		{
			for(JobProbe probe : _probes) {
				currentStep += probe.getCurrentStep();
				maxStep     += probe.getMaxStep    ();
			}
		}
		
		// Then, the progress is given by the ratio currentStep/maxStep clamped
		// between 0 and 1.
		if(maxStep<=0) {
			return -1;
		}
		else {
			double progress = (double)currentStep / (double)maxStep;
			if(progress>=1.0) {
				return 1.0;
			}
			else if(progress<=0.0) {
				return 0.0;
			}
			else {
				return progress;
			}
		}
	}
	
	/**
	 * Method to call to ask the convert function to stop the computation before it finishes
	 * @warning Calling this method does not mean the computation will actually be interrupted.
	 *          The computation is interrupted if a CanceledByUser exception is thrown.
	 */
	public void cancelComputation()
	{
		_cancelFlag = true;
	}
	
	/**
	 * Check-point function called by the computation function
	 */
	public void checkPoint() throws CanceledByUser
	{
		if(_cancelFlag) {
			throw new CanceledByUser();
		}
	}
}
