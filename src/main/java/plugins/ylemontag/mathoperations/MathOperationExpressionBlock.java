package plugins.ylemontag.mathoperations;

import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.util.VarException;
import plugins.adufour.vars.util.VarListener;
import plugins.adufour.vars.util.VarReferencingPolicy;

/**
 * @author Yoann Le Montagner Parametric block with a list of inputs dependent of a Math expression
 */
public class MathOperationExpressionBlock extends MathOperationAbstractBlock
{
    private VarExpression _expression;
    
    private String[] _names;
    
    public MathOperationExpressionBlock()
    {
        super("result");
        _expression = new VarExpression("", null);
        _expression.setReferencingPolicy(VarReferencingPolicy.NONE);
    }
    
    @Override
    public void declareInput(final VarList inputMap)
    {
        _expression.addListener(new VarListener<Expression>()
        {
            @Override
            public void valueChanged(Var<Expression> source, Expression oldValue, Expression newValue)
            {
                _names = newValue == null ? null : newValue.getVariablesAsArray();
                defineRuntimeInputVariants(inputMap, _names);
            }
            
            @Override
            public void referenceChanged(Var<Expression> source, Var<? extends Expression> oldReference, Var<? extends Expression> newReference)
            {
            }
        });
        inputMap.add("Operation", _expression);
    }
    
    @Override
    public String getMainPluginClassName()
    {
        return MathOperationPlugin.class.getName();
    }
    
    @Override
    public String getName()
    {
        return "Math expression";
    }
    
    @Override
    public void run()
    {
     // Retrieve the input values
        Functor   fun    = extractFunctor();
        Variant[] in     = new Variant[_names.length];
        String [] inDesc = new String [_names.length];
        for(int k=0; k<_names.length; ++k) {
            in    [k] = retrieveInputValue(_names[k]);
            inDesc[k] = in[k].toString();
        }
        String description = fun.describeOperation(inDesc);
        
        // Execute the operation and save the value
        try {
            Variant out = fun.apply(in);
            defineOutputValue(out);
        }
        catch(Functor.InconsistentArguments err) {
            reportError(err, description);
        }
    }
    
    /**
     * Extract the functor
     */
    private Functor extractFunctor()
    {
        Expression expression = _expression.getValue();
        if(expression==null) {
            throw new VarException("The 'op' field is either empty or inconsistent.");
        }
        try {
            return expression.getFunctor(_names);
        }
        catch(Expression.BadFunctor err) {
            throw new VarException(
                "Cannot interpret the 'op' field. Here is the error message: " + err.getMessage()
            );
        }
    }
}
