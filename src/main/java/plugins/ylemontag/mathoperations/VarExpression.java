package plugins.ylemontag.mathoperations;

import javax.swing.JComponent;

import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Algebraic expression wrapped into in the Vars framework
 */
public class VarExpression extends Var<Expression>
{
	/**
	 * Editor
	 */
	private static class Editor extends SwingVarEditor<Expression>
	{
		private ExpressionComponent _component;
		private boolean _shuntVariableUpdate ;
		private boolean _shuntComponentUpdate;
		
		public Editor(Var<Expression> variable)
		{
			super(variable);
			_shuntVariableUpdate  = false;
			_shuntComponentUpdate = false;
		}
		
		@Override
		protected JComponent createEditorComponent()
		{
			_component = new ExpressionComponent();
			_component.addValueListener(new ExpressionComponent.ValueListener()
			{
				@Override
				public void valueChanged(Expression newValue)
				{
					if(_shuntVariableUpdate) {
						return;
					}
					_shuntComponentUpdate = true;
					variable.setValue(newValue);
					_shuntComponentUpdate = false;
				}
			});
			return _component;
		}
		
		@Override
		protected void activateListeners()
		{
			_shuntVariableUpdate = false;
		}
		
		@Override
		protected void deactivateListeners()
		{
			_shuntVariableUpdate = true;
		}
		
		@Override
		protected void updateInterfaceValue()
		{
			if(_shuntComponentUpdate) {
				return;
			}
			_component.setValue(variable.getValue());
		}		
	}
	
	public VarExpression(String name, Expression defaultValue)
	{
		super(name, Expression.class, defaultValue);
	}
	
	@Override
	public VarEditor<Expression> createVarEditor()
	{
		return new Editor(this);
	}
	
	@Override
	public Expression parse(String text)
	{
		if(text.equals("null")) {
			return null;
		}
		else {
			try {
				return Expression.parse(text);
			}
			catch(Expression.ParsingException e) {
				return null;
			}
		}
	}
}
