package plugins.ylemontag.mathoperations.functors;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Functor classes generation script
 */
public class AutoGenerateApplyMethods
{
	private static int methodCounter = 0;
	
	public static void main(String[] args)
	{
		// Number of input arguments in the apply methods()
		int N = 1;
		// The number of apply methods is supposed to be (4*(3^N + 2^N) - 7):
		//  N=1 ->  13 methods
		//  N=2 ->  45 methods
		//  N=3 -> 133 methods
		//  N=4 -> 381 methods
		
		// Set to true to generate the apply methods of classes OperationN,
		// and to false for those of classes FunctorN
		boolean inOperation = false;
		
		// Output file
		String fileName = "out.txt";
		
		// First template: no pre-allocated result, no controller
		String tplApply1 = 
			"/**\n" +
			" * Specialized evaluation function\n" +
			" * @see Functor%1$s.apply(%2$s)\n" +
			" */\n" +
			"public %3$s apply(%4$s)\n" +
			"{\n" +
			(inOperation
				? "\treturn _functor.apply(%7$s);\n"
				: "\treturn apply(%5$s).%6$s;\n"
			) +
			"}\n";
		LinkedList<MethodType> methods1 = makeCombinations(N, true);
		
		// Second template: pre-allocated result, no controller
		String tplApply2 = 
			"/**\n" +
			" * Specialized evaluation function\n" +
			" * @see Functor%1$s.apply(Variant out, %2$s)\n" +
			" */\n" +
			"public void apply(%3$s out, %4$s)\n" +
			"{\n" +
			(inOperation
				? "\t_functor.apply(out, %7$s);\n"
				: "\tapply(Variant.wrap(out), %5$s);\n"
			) +
			"}\n";
		LinkedList<MethodType> methods2 = makeCombinations(N, false);
		
		// Third template: no pre-allocated result, controller
		String tplApply3 = 
			"/**\n" +
			" * Specialized evaluation function\n" +
			" * @see Functor%1$s.apply(%2$s, Controller controller)\n" +
			" */\n" +
			"public %3$s apply(%4$s, Controller controller)\n" +
			"\tthrows Controller.CanceledByUser\n" +
			"{\n" +
			(inOperation
				? "\treturn _functor.apply(%7$s, controller);\n"
				: "\treturn apply(%5$s, controller).%6$s;\n"
			) +
			"}\n";
		LinkedList<MethodType> methods3 = makeCombinations(N, false);
		
		// Fourth template: pre-allocated result, controller
		String tplApply4 = 
			"/**\n" +
			" * Specialized evaluation function\n" +
			" * @see Functor%1$s.apply(Variant out, %2$s, Controller controller)\n" +
			" */\n" +
			"public void apply(%3$s out, %4$s, Controller controller)\n" +
			"\tthrows Controller.CanceledByUser\n" +
			"{\n" +
			(inOperation
				? "\t_functor.apply(out, %7$s, controller);\n"
				: "\tapply(Variant.wrap(out), %5$s, controller);\n"
			) +
			"}\n";
		LinkedList<MethodType> methods4 = makeCombinations(N, false);
		
		// Write the output file
		try
		{
			FileWriter stream = new FileWriter(fileName);
			try
			{
				methodCounter = 0;
				printMethodFamily(stream, tplApply1, methods1);
				printMethodFamily(stream, tplApply2, methods2);
				printMethodFamily(stream, tplApply3, methods3);
				printMethodFamily(stream, tplApply4, methods4);
			}
			finally {
				stream.close();
			}
			System.out.println("Done.");
			System.out.println("Method count: " + methodCounter);
		}
		catch(IOException err) {
			System.err.println("I/O error: " + err.getMessage());
		}
	}
	
	/**
	 * String code 1: number of arguments
	 */
	private static String code1(MethodType method)
	{
		return Integer.toString(method.input.length);
	}
	
	/**
	 * String code 2: "Variant in1, ..., Variant inN"
	 */
	private static String code2(MethodType method)
	{
		String retVal = "";
		for(int k=1; k<=method.input.length; ++k) {
			if(k>=2) {
				retVal += ", ";
			}
			retVal += "Variant in" + k;
		}
		return retVal;
	}
	
	/**
	 * String code 3: type of the 'out' variable
	 */
	private static String code3(MethodType method)
	{
		return toJavaType(method.output);
	}
	
	/**
	 * String code 4: list of typed inputs
	 */
	private static String code4(MethodType method)
	{
		String retVal = "";
		for(int k=1; k<=method.input.length; ++k) {
			if(k>=2) {
				retVal += ", ";
			}
			retVal += toJavaType(method.input[k-1]) + " in" + k;
		}
		return retVal;
	}
	
	/**
	 * String code 5: "Variant.wrap(in1), ..., Variant.wrap(inN)"
	 */
	private static String code5(MethodType method)
	{
		String retVal = "";
		for(int k=1; k<=method.input.length; ++k) {
			if(k>=2) {
				retVal += ", ";
			}
			retVal += "Variant.wrap(in" + k + ")";
		}
		return retVal;
	}
	
	/**
	 * String code 6: output conversion function
	 */
	private static String code6(MethodType method)
	{
		return toGetAsMethod(method.output);
	}
	
	/**
	 * String code 7: "in1, ..., inN"
	 */
	private static String code7(MethodType method)
	{
		String retVal = "";
		for(int k=1; k<=method.input.length; ++k) {
			if(k>=2) {
				retVal += ", ";
			}
			retVal += "in" + k;
		}
		return retVal;
	}
	
	/**
	 * Convert a variant type to its java type equivalent
	 */
	private static String toJavaType(Variant.Type type)
	{
		switch(type) {
			case SCALAR     : return "double"     ;
			case ARRAY      : return "double[]"   ;
			case SEQUENCE   : return "Sequence"   ;
			case SUBSEQUENCE: return "SubSequence";
			default:
				throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Convert a variant type to its conversion function in the Variant class
	 */
	private static String toGetAsMethod(Variant.Type type)
	{
		switch(type) {
			case SCALAR     : return "getAsScalar()"  ;
			case ARRAY      : return "getAsArray()"   ;
			case SEQUENCE   : return "getAsSequence()";
			case SUBSEQUENCE: throw new IllegalArgumentException("No getAs-method for sub-sequences.");
			default:
				throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Write all the methods corresponding to the given template to the stream
	 */
	private static void printMethodFamily(Writer stream, String template, List<MethodType> methods)
		throws IOException
	{
		for(MethodType method : methods) {
			String fullCode = String.format(template, code1(method), code2(method), code3(method),
				code4(method), code5(method), code6(method), code7(method));
			stream.write("\n" + fullCode);
			++methodCounter;
		}
	}
	
	/**
	 * Generate all the variant types combinations
	 */
	private static LinkedList<MethodType> makeCombinations(int n, boolean withOutputScalar)
	{
		LinkedList<MethodType> retVal = new LinkedList<MethodType>();
		
		// Output: scalar
		if(withOutputScalar) {
			LinkedList<MethodType> outputScalar = makeCombinations(Variant.Type.SCALAR, n, Variant.Type.SCALAR);
			retVal.addAll(outputScalar);
		}
		
		// Output: array
		LinkedList<MethodType> outputArray = makeCombinations(Variant.Type.ARRAY, n,
			Variant.Type.SCALAR, Variant.Type.ARRAY);
		outputArray.removeFirst();
		retVal.addAll(outputArray);
		
		// Output: sequence
		LinkedList<MethodType> outputSeq = makeCombinations(Variant.Type.SEQUENCE, n,
			Variant.Type.SCALAR, Variant.Type.SEQUENCE, Variant.Type.SUBSEQUENCE);
		outputSeq.removeFirst();
		retVal.addAll(outputSeq);
		
		// Result
		return retVal;
	}
	
	/**
	 * Generate all the combinations input types 'in', with output type 'out'
	 */
	private static LinkedList<MethodType> makeCombinations(Variant.Type out, int n, Variant.Type ... in)
	{
		LinkedList<MethodType> retVal = new LinkedList<MethodType>();
		Variant.Type[] buffer = new Variant.Type[n];
		auxMakeCombinations(retVal, out, buffer, 0, in);
		return retVal;
	}
	
	/**
	 * Auxilliary function
	 */
	private static void auxMakeCombinations(LinkedList<MethodType> retVal, Variant.Type out,
		Variant.Type[] buffer, int k, Variant.Type[] in)
	{
		if(k>=buffer.length) {
			retVal.addLast(new MethodType(out, buffer));
		}
		else {
			for(Variant.Type t : in) {
				buffer[k] = t;
				auxMakeCombinations(retVal, out, buffer, k+1, in);
			}
		}
	}
	
	/**
	 * Specify the types of a set of inputs + an output
	 */
	private static class MethodType
	{
		public Variant.Type   output;
		public Variant.Type[] input ;
		
		public MethodType(Variant.Type out, Variant.Type[] in)
		{
			output = out;
			input  = new Variant.Type[in.length];
			for(int k=0; k<in.length; ++k) {
				input[k] = in[k];
			}
		}
	}
}
