package plugins.ylemontag.mathoperations.functors;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.Controller;
import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;
import plugins.ylemontag.mathoperations.SubSequence;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * 3 argument functor
 */
public class Functor3 extends Functor
{
	/**
	 * Try to interpret the string 'expr' as a function with 3 input variables named 'v1', 'v2' and 'v3'
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor3 parse(String expr, String v1, String v2, String v3)
	{
		return Expression.parse(expr).getFunctor3(v1, v2, v3);
	}
	
	/**
	 * Try to interpret the string 'expr' as a function with 3 input variables
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor3 parse(String expr)
	{
		return Expression.parse(expr).getFunctor3();
	}
	
	/**
	 * Constructor
	 */
	public Functor3(String describePattern, FunPtr functionPointer)
	{
		super(3, describePattern, functionPointer);
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 */
	public String describeOperation(String in1, String in2, String in3)
	{
		return describeOperation(new String[] { in1, in2, in3 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 */
	public Variant apply(Variant in1, Variant in2, Variant in3)
	{
		return apply(new Variant[] { in1, in2, in3 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 */
	public void apply(Variant out, Variant in1, Variant in2, Variant in3)
	{
		apply(out, new Variant[] { in1, in2, in3 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public Variant apply(Variant in1, Variant in2, Variant in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(new Variant[] { in1, in2, in3 }, controller);
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public void apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(out, new Variant[] { in1, in2, in3 }, controller);
	}
	
	/*############################################################################
	  # BELOW: auto-generated functions                                        #
	############################################################################*/
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double apply(double in1, double in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsScalar();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double in1, double in2, double[] in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double in1, double[] in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double in1, double[] in2, double[] in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double[] in1, double in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double[] in1, double in2, double[] in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double[] in1, double[] in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public double[] apply(double[] in1, double[] in2, double[] in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, double in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, double in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, Sequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, Sequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, Sequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, SubSequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, SubSequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(double in1, SubSequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, double in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, double in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, double in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, Sequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, Sequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, Sequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, double in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, double in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, double in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, double in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, Sequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, SubSequence in3)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double in1, double in2, double[] in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double in1, double[] in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double in1, double[] in2, double[] in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double[] in1, double in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double[] in1, double in2, double[] in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double[] in1, double[] in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(double[] out, double[] in1, double[] in2, double[] in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, double in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, double in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, Sequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, Sequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, Sequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, double in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, double in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, double in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, double in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, Sequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, SubSequence in3)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double in1, double in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double in1, double[] in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double in1, double[] in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double[] in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double[] in1, double in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double[] in1, double[] in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public double[] apply(double[] in1, double[] in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(double in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double in1, double in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double in1, double[] in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double in1, double[] in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double[] in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double[] in2, double[] in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, double in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, Sequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Variant in3, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, SubSequence in3, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), Variant.wrap(in3), controller);
	}
}
