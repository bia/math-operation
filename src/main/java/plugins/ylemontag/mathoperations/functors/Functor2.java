package plugins.ylemontag.mathoperations.functors;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.Controller;
import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;
import plugins.ylemontag.mathoperations.SubSequence;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * 2 argument functor
 */
/**
 * @author Stephane
 *
 */
public class Functor2 extends Functor
{
	/**
	 * Try to interpret the string 'expr' as a function with 2 input variables named 'v1' and 'v2'
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor2 parse(String expr, String v1, String v2)
	{
		return Expression.parse(expr).getFunctor2(v1, v2);
	}
	
	/**
	 * Try to interpret the string 'expr' as a function with 2 input variables
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor2 parse(String expr)
	{
		return Expression.parse(expr).getFunctor2();
	}
	
	/**
	 * Constructor
	 */
	public Functor2(String describePattern, FunPtr functionPointer)
	{
		super(2, describePattern, functionPointer);
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 */
	public String describeOperation(String in1, String in2)
	{
		return describeOperation(new String[] { in1, in2 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 */
	public Variant apply(Variant in1, Variant in2)
	{
		return apply(new Variant[] { in1, in2 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 */
	public void apply(Variant out, Variant in1, Variant in2)
	{
		apply(out, new Variant[] { in1, in2 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public Variant apply(Variant in1, Variant in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(new Variant[] { in1, in2 }, controller);
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public void apply(Variant out, Variant in1, Variant in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(out, new Variant[] { in1, in2 }, controller);
	}
	
	/*############################################################################
	  # BELOW: auto-generated functions                                        #
	############################################################################*/
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public double apply(double in1, double in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsScalar();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public double[] apply(double in1, double[] in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public double[] apply(double[] in1, double in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public double[] apply(double[] in1, double[] in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(double in1, Sequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(double in1, SubSequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, double in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, Sequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, SubSequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, double in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, Sequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @param in1 
	 * @param in2 
	 * @return 
	 * @see #apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2)
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double in1, double[] in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double[] in1, double in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double[] in1, double[] in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, double in1, Sequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, double in1, SubSequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, double in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, double in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2)
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double[] in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double[] in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(double in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(double in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), Variant.wrap(in2), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, double in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), Variant.wrap(in2), controller);
	}
}
