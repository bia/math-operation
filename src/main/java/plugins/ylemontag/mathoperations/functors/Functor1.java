package plugins.ylemontag.mathoperations.functors;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.Controller;
import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;
import plugins.ylemontag.mathoperations.SubSequence;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * 1 argument functor
 */
public class Functor1 extends Functor
{
	/**
	 * Try to interpret the string 'expr' as a function with 1 input variable named 'v1'
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor1 parse(String expr, String v1)
	{
		return Expression.parse(expr).getFunctor1(v1);
	}
	
	/**
	 * Try to interpret the string 'expr' as a function with 1 input variable
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor1 parse(String expr)
	{
		return Expression.parse(expr).getFunctor1();
	}
	
	/**
	 * Constructor
	 */
	public Functor1(String describePattern, FunPtr functionPointer)
	{
		super(1, describePattern, functionPointer);
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 */
	public String describeOperation(String in1)
	{
		return describeOperation(new String[] { in1 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 */
	public Variant apply(Variant in1)
	{
		return apply(new Variant[] { in1 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 */
	public void apply(Variant out, Variant in1)
	{
		apply(out, new Variant[] { in1 });
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public Variant apply(Variant in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(new Variant[] { in1 }, controller);
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'out'
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public void apply(Variant out, Variant in1, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(out, new Variant[] { in1 }, controller);
	}
	
	/*############################################################################
	  # BELOW: auto-generated functions                                        #
	############################################################################*/
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1)
	 */
	public double apply(double in1)
	{
		return apply(Variant.wrap(in1)).getAsScalar();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1)
	 */
	public double[] apply(double[] in1)
	{
		return apply(Variant.wrap(in1)).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1)
	 */
	public Sequence apply(Sequence in1)
	{
		return apply(Variant.wrap(in1)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1)
	 */
	public Sequence apply(SubSequence in1)
	{
		return apply(Variant.wrap(in1)).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1)
	 */
	public void apply(double[] out, double[] in1)
	{
		apply(Variant.wrap(out), Variant.wrap(in1));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1)
	 */
	public void apply(Sequence out, Sequence in1)
	{
		apply(Variant.wrap(out), Variant.wrap(in1));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1)
	 */
	public void apply(Sequence out, SubSequence in1)
	{
		apply(Variant.wrap(out), Variant.wrap(in1));
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Controller controller)
	 */
	public double[] apply(double[] in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), controller).getAsArray();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Controller controller)
	 */
	public Sequence apply(Sequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant in1, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return apply(Variant.wrap(in1), controller).getAsSequence();
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Controller controller)
	 */
	public void apply(double[] out, double[] in1, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see #apply(Variant out, Variant in1, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		apply(Variant.wrap(out), Variant.wrap(in1), controller);
	}
}
