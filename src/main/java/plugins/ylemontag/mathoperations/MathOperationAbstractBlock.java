package plugins.ylemontag.mathoperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.BlockInfo;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.util.TypeChangeListener;
import plugins.adufour.vars.util.VarException;

/**
 * @author Yoann Le Montagner
 *         Block for binary operations
 */
public abstract class MathOperationAbstractBlock extends Plugin implements Block, BlockInfo, PluginBundled
{
    private VarMutable _out;
    private Variant.DimensionType _outType;
    private Set<String> _staticVars;
    private Map<String, VarVariant> _inputs;

    /**
     * Constructor
     */
    protected MathOperationAbstractBlock(String outLabel)
    {
        _out = new VarMutable(outLabel, null);
        _outType = null;
        _staticVars = new HashSet<String>();
        _inputs = new HashMap<String, VarVariant>();
    }

    /**
     * Add a non-runtime input variable
     */
    protected void addInputVariant(VarList inputMap, String name, String uid)
    {
        _staticVars.add(name);
        inputMap.add(uid, new VarVariant(name));
    }

    /**
     * Define the list of runtime inputs
     */
    protected void defineRuntimeInputVariants(VarList inputMap, String[] names)
    {
        // Create the new input variables
        if (names != null)
        {
            for (String name : names)
            {
                // Nothing to do if the input variable already exists
                if (_inputs.get(name) != null)
                {
                    continue;
                }

                // Otherwise, create a new input variable
                VarVariant v = new VarVariant(name);
                inputMap.addRuntimeVariable("dyn-" + name, v);
            }
        }

        // Remove the old input variables that are no longer in use
        Set<String> toBeRemoved = getOutdatedVariables(names);
        for (String name : toBeRemoved)
        {
            VarVariant v = _inputs.get(name);
            inputMap.remove(v);
            _inputs.remove(name);
        }

        // Refresh the type of the output variable
        refreshOutputType();
    }

    /**
     * Return a set that contains a list of outdated variables, i.e. those
     * listed in _inputs but neither in _staticVars nor in runtimeVariables
     */
    private Set<String> getOutdatedVariables(String[] runtimeVariables)
    {
        Set<String> retVal = new HashSet<String>();
        for (String name : _inputs.keySet())
        {
            // Look in the list of static variables...
            if (_staticVars.contains(name))
            {
                continue;
            }

            // ... and in the list of runtime variables
            if (runtimeVariables != null)
            {
                boolean inRuntimeVariables = false;
                for (String s : runtimeVariables)
                {
                    if (name.equals(s))
                    {
                        inRuntimeVariables = true;
                        break;
                    }
                }
                if (inRuntimeVariables)
                {
                    continue;
                }
            }

            // Flag the variable as outdated
            retVal.add(name);
        }
        return retVal;
    }

    /**
     * Extract the value corresponding to an input
     * 
     * @throws VarException
     *         If no object is mapped to the corresponding input.
     * @throws IllegalArgumentException
     *         If the given name does not correspond to an already defined variable.
     */
    protected Variant retrieveInputValue(String name)
    {
        VarVariant v = _inputs.get(name);
        if (v == null)
        {
            throw new IllegalArgumentException("Undeclared variable name: " + name + ".");
        }
        return v.getVariant();
    }

    /**
     * Update the output value
     */
    protected void defineOutputValue(Variant value)
    {
        switch (value.getType())
        {
            case SCALAR:
                _out.setValue(value.getAsScalar());
                break;
            case ARRAY:
                _out.setValue(value.getAsArray());
                break;
            case SEQUENCE:
                _out.setValue(value.getAsSequence());
                break;
            default:
                throw new RuntimeException("Unreachable code point");
        }
    }

    /**
     * Report an error
     * @param description String
     * @param err Functor
     */
    protected static void reportError(Functor.InconsistentArguments err, String description)
    {
        throw new VarException("Error while executing the operation " + description + ": " + err.getMessage());
    }

    /**
     * Determine the output type based on the input types
     * @return Variant
     */
    private Variant.DimensionType computeOutputType()
    {
        // If only one input has an undefined type, the type of the output is kept undefined
        for (VarVariant v : _inputs.values())
        {
            if (v.getVariantType() == null)
            {
                return null;
            }
        }

        // Otherwise, extract the type of all the inputs
        Variant.Type[] inputTypes = new Variant.Type[_inputs.size()];
        int k = 0;
        for (VarVariant v : _inputs.values())
        {
            inputTypes[k] = v.getVariantType();
            ++k;
        }

        // Return the result
        return Functor.computeOutputType(inputTypes);
    }

    /**
     * Re-compute the theoretical type of the output based on the types of the
     * inputs, and updated it if necessary
     */
    private void refreshOutputType()
    {
        // Determine the output type
        Variant.DimensionType newType = computeOutputType();

        // Nothing to do if the output already have the right type
        if (_outType == newType)
        {
            return;
        }

        // Disconnect the variable if necessary
        if (_out.isReferenced())
        {
            // remove this debug info
            // System.out.println("Out is referenced");
            
            // do that to avoid ConcurrentModificationException
            final Iterator outReferrersIt = _out.getReferrers();
            final List<Var<?>> outReferrers = new ArrayList<>();
            while (outReferrersIt.hasNext())
                outReferrers.add((Var) outReferrersIt.next());

            for (Var<?> varCasted : outReferrers)
            {
                System.out.println("Remove ref on " + varCasted.getName()); /// TODO: debug
                varCasted.setReference(null);
            }
        }
        else
        {
            // remove this debug info
            // System.out.println("Out not is referenced");
        }

        // Set the type
        _outType = newType;
        if (_outType == null)
        {
            _out.setType(null);
        }
        else
        {
            switch (_outType)
            {
                case SCALAR:
                    _out.setType(Double.class);
                    _out.setValue((Double) 0.0);
                    break;
                case ARRAY:
                    _out.setType(double[].class);
                    _out.setValue(new double[0]);
                    break;
                case SEQUENCE:
                    _out.setType(Sequence.class);
                    _out.setValue(null);
                    break;
                default:
                    throw new RuntimeException("Unreachable code point");
            }
        }
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Out", _out);
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    /**
     * Override VarMutable to manage which variables can be assigned
     */
    private class VarVariant extends VarMutable
    {
        private Variant.Type _variantType;

        /**
         * Constructor
         */
        public VarVariant(String name)
        {
            super(name, null);
            addTypeChangeListener(new TypeChangeListener()
            {
                @Override
                public void typeChanged(Object source, Class<?> oldType, Class<?> newType)
                {
                    updateVariantType(newType);
                    refreshOutputType();
                }
            });
            _inputs.put(name, this);
        }

        /**
         * Return the variant type
         */
        public Variant.Type getVariantType()
        {
            return _variantType;
        }

        /**
         * Return the variant
         */
        public Variant getVariant()
        {
            if (_variantType == null)
            {
                throw new VarException("No variable linked to the input '" + getName() + "'");
            }
            else
            {
                switch (_variantType)
                {
                    case SCALAR:
                        return Variant.wrap(getScalar());
                    case ARRAY:
                        return Variant.wrap(getArray());
                    case SEQUENCE:
                        return Variant.wrap(getSequence());
                    default:
                        throw new RuntimeException("Unreachable code point");
                }
            }
        }

        @SuppressWarnings("rawtypes")
        @Override
        public boolean isAssignableFrom(Var source)
        {
            Class<?> sourceType = source.getType();
            if (_variantType == null)
            {
                if (isScalarType(sourceType))
                    return willVariantTypeBeConsistent(Variant.Type.SCALAR);
                else if (isArrayType(sourceType))
                    return willVariantTypeBeConsistent(Variant.Type.ARRAY);
                else if (isSequenceType(sourceType))
                    return willVariantTypeBeConsistent(Variant.Type.SEQUENCE);
                else
                    return false;
            }
            else
            {
                switch (_variantType)
                {
                    case SCALAR:
                        return isScalarType(sourceType);
                    case ARRAY:
                        return isArrayType(sourceType);
                    case SEQUENCE:
                        return isSequenceType(sourceType);
                    default:
                        throw new RuntimeException("Unreachable code point");
                }
            }
        }

        /**
         * Check if updating the local variant type to 'futureType' will be coherent
         * with respect to the other input variables
         */
        private boolean willVariantTypeBeConsistent(Variant.Type futureType)
        {
            // Extract the type of all the inputs
            Variant.Type[] inputTypes = new Variant.Type[_inputs.size()];
            int k = 0;
            for (VarVariant v : _inputs.values())
            {
                inputTypes[k] = (v == this) ? futureType : v.getVariantType();
                ++k;
            }

            // Check the consistency
            return Functor.isConsistentInputTypes(inputTypes);
        }

        /**
         * Update the variant type
         */
        private void updateVariantType(Class<?> type)
        {
            if (type == null)
                _variantType = null;
            else if (isScalarType(type))
                _variantType = Variant.Type.SCALAR;
            else if (isArrayType(type))
                _variantType = Variant.Type.ARRAY;
            else if (isSequenceType(type))
                _variantType = Variant.Type.SEQUENCE;
            else
            {
                throw new IllegalStateException("Illegal affectation: trying to link '" + getName()
                        + "' to a variable with type " + type.getName());
            }
        }

        /**
         * Check whether the given type is admissible as a scalar input
         */
        private boolean isScalarType(Class<?> type)
        {
            return type == Double.class || type == Float.class || type == Integer.class || type == Double.TYPE
                    || type == Float.TYPE || type == Integer.TYPE;
        }

        /**
         * Check whether the given type is admissible as an array input
         */
        private boolean isArrayType(Class<?> type)
        {
            return type == double[].class || type == float[].class || type == int[].class;
        }

        /**
         * Check whether the given type is admissible as a sequence input
         */
        private boolean isSequenceType(Class<?> type)
        {
            return type == Sequence.class;
        }

        /**
         * Extract a scalar value
         */
        private double getScalar()
        {
            Class<?> type = getType();
            if (type == Double.class || type == Double.TYPE)
                return (Double) getValue(true);
            else if (type == Float.class || type == Float.TYPE)
                return (Float) getValue(true);
            else if (type == Integer.class || type == Integer.TYPE)
                return (Integer) getValue(true);
            else
            {
                throw new IllegalArgumentException(
                        "Cannot interpret '" + getName() + "' as a scalar value as it has type " + type.getName());
            }
        }

        /**
         * Extract an array value
         */
        private double[] getArray()
        {
            Class<?> type = getType();
            if (type == double[].class)
                return (double[]) getValue(true);
            else if (type == float[].class)
                return Array1DUtil.floatArrayToDoubleArray((float[]) getValue(true));
            else if (type == int[].class)
                return Array1DUtil.intArrayToDoubleArray((int[]) getValue(true), true);
            else
            {
                throw new IllegalArgumentException(
                        "Cannot interpret '" + getName() + "' as an array as it has type " + type.getName());
            }
        }

        /**
         * Extract a sequence value
         */
        private Sequence getSequence()
        {
            Class<?> type = getType();
            if (type == Sequence.class)
            {
                return (Sequence) getValue(true);
            }
            else
            {
                throw new IllegalArgumentException(
                        "Cannot interpret '" + getName() + "' as a sequence as it has type " + type.getName());
            }
        }
    }
}
