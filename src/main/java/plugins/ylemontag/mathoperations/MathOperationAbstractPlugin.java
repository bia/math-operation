package plugins.ylemontag.mathoperations;

import icy.gui.frame.progress.AnnounceFrame;
import icy.main.Icy;
import icy.sequence.DimensionId;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.Timer;

import plugins.adufour.ezplug.EzGUI;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarDoubleArrayNative;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.ylemontag.mathoperations.Variant.Type;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Base class for plugins that execute functors
 */
public abstract class MathOperationAbstractPlugin extends EzPlug implements EzStoppable
{
	private Timer      _timer     ;
	private Controller _controller;
	
	/**
	 * Constructor
	 */
	protected MathOperationAbstractPlugin()
	{
		_timer = new Timer(100, new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				refreshProgressBar();
			}
		});
		_timer.start();
	}
	
	/**
	 * Display the result
	 */
	private void showOutput(Variant out, String description)
	{
		switch(out.getType())
		{
			// Display an announce frame with the result for scalar and array values
			case SCALAR:
			case ARRAY :
				new AnnounceFrame(description + " = " + out);
				break;
			
			// Open the result if it is a sequence
			case SEQUENCE:
				Icy.getMainInterface().addSequence(out.getAsSequence());
				break;
			
			// This case should not arise
			default:
				throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Call the functor
	 * @param functor Functor
	 * @param inputs EZGroupVariant
	 */
	protected void coreExecute(Functor functor, EzGroupVariant ... inputs)
	{
		// Retrieve the inputs
		Variant[] variants = new Variant[inputs.length];
		String [] names    = new String [inputs.length];
		for(int k=0; k<inputs.length; ++k) {
			variants[k] = inputs[k].getInput();
			names   [k] = variants[k].getRepresentation(true);
		}
		
		// Execute the operation
		String description = functor.describeOperation(names);
		_controller = new Controller();
		try
		{
			Variant out = functor.apply(variants, _controller);
			showOutput(out, description);
		}
		catch(Functor.InconsistentArguments err) {
			throw new IcyHandledException(
				"Error while executing the operation " + description + ".\n" + err.getMessage()
			);
		}
		catch(Controller.CanceledByUser err) {
			new AnnounceFrame("The operation " + description + " has been canceled");
		}
		finally {
			_controller = null;
		}
	}
	
	@Override
	public void clean()
	{
		_timer.stop();
	}
	
	@Override
	public void stopExecution()
	{
		Controller controller = _controller;
		if(controller!=null) {
			controller.cancelComputation();
		}
	}
	
	/**
	 * Refresh the progress bar with the progress value in held by the controller, if available
	 */
	private void refreshProgressBar()
	{
		EzGUI gui = getUI();
		if(gui==null) {
			return;
		}
		Controller controller = _controller;
		gui.setProgressBarValue(controller==null ? 0.0 : controller.getProgress());
	}
	
	/** 
	 * Group of EzComponent to select a Variant object
	 */
	protected static class EzGroupVariant extends EzGroup
	{
		private String                  _label ;
		private EzVarEnum<Variant.Type> _type  ;
		private EzVarDouble             _scalar;
		private EzVarDoubleArrayNative  _array ;
		private EzVarSequence           _seq   ;
		private EzVarDimensionsPicker   _dimT  ;
		private EzVarDimensionsPicker   _dimZ  ;
		private EzVarDimensionsPicker   _dimC  ;
		
		/**
		 * Constructor
		 */
		public EzGroupVariant(String label)
		{
			this(label,
				new EzVarEnum<Variant.Type>("Type" , makeAvailableTypes(), Variant.Type.SEQUENCE),
				new EzVarDouble           ("Scalar"  , 0.0, -Double.MAX_VALUE, Double.MAX_VALUE, 0.01),
				new EzVarDoubleArrayNative("Array"   , null, true),
				new EzVarSequence         ("Sequence")
			);
		}
		
		/**
		 * Actual constructor
		 */
		private EzGroupVariant(String label, EzVarEnum<Variant.Type> type, EzVarDouble scalar,
			EzVarDoubleArrayNative array, EzVarSequence seq)
		{
			this(label, type, scalar, array, seq,
				new EzVarDimensionsPicker("T", DimensionId.T, seq),
				new EzVarDimensionsPicker("Z", DimensionId.Z, seq),
				new EzVarDimensionsPicker("C", DimensionId.C, seq)
			);
		}
		
		/**
		 * Actual constructor
		 */
		private EzGroupVariant(String label, EzVarEnum<Variant.Type> type, EzVarDouble scalar,
			EzVarDoubleArrayNative array, EzVarSequence seq, EzVarDimensionsPicker dimT,
			EzVarDimensionsPicker dimZ, EzVarDimensionsPicker dimC)
		{
			super(label, type, scalar, array, seq, dimT, dimZ, dimC);
			_label  = label ;
			_type   = type  ;
			_scalar = scalar;
			_array  = array ;
			_seq    = seq   ;
			_dimT   = dimT  ;
			_dimZ   = dimZ  ;
			_dimC   = dimC  ;
			_type.addVisibilityTriggerTo(_scalar, Variant.Type.SCALAR  );
			_type.addVisibilityTriggerTo(_array , Variant.Type.ARRAY   );
			_type.addVisibilityTriggerTo(_seq   , Variant.Type.SEQUENCE);
			refreshDimensionsPickerVisibility();
			_type.addVarChangeListener(new EzVarListener<Variant.Type>()
			{
				@Override
				public void variableChanged(EzVar<Type> source, Type newValue)
				{
					refreshDimensionsPickerVisibility();
				}
			});
			_seq.addVarChangeListener(new EzVarListener<Sequence>()
			{
				@Override
				public void variableChanged(EzVar<Sequence> source, Sequence newValue)
				{
					refreshDimensionsPickerVisibility();
				}
			});
		}
		
		/**
		 * @return Retrieve the input argument
		 */
		public Variant getInput()
		{
			switch(_type.getValue()) {
				case SCALAR  : return Variant.wrap(_scalar.getValue(true));
				case ARRAY   : return Variant.wrap(_array .getValue(true));
				case SEQUENCE: return getSequenceOrSubSequence();
				default: throw new RuntimeException("Unreachable code point");
			}
		}
		
		/**
		 * Retrieve the selected sub-sequence, or the sequence in case of full-selection
		 */
		private Variant getSequenceOrSubSequence()
		{
			SubSequence subSeq = new SubSequence(_seq.getValue(true));
			subSeq.setT(_dimT.getValidatedIndexRange());
			subSeq.setZ(_dimZ.getValidatedIndexRange());
			subSeq.setC(_dimC.getValidatedIndexRange());
			if(!subSeq.isValid()) {
				throw new IcyHandledException(
					"The selected range of index in the sequence " + subSeq.getSequence().getName() +
					" (variable '" + _label + "') is not valid."
				);
			}
			if(subSeq.isFullySelected()) {
				return Variant.wrap(subSeq.getSequence());
			}
			else {
				return Variant.wrap(subSeq);
			}
		}
		
		/**
		 * Refresh the visibility of the dimension pickers component
		 */
		private void refreshDimensionsPickerVisibility()
		{
			boolean sequenceIsVisible = (_type.getValue()==Variant.Type.SEQUENCE);
			_dimT.setVisible(sequenceIsVisible && _dimT.shouldBeVisible());
			_dimZ.setVisible(sequenceIsVisible && _dimZ.shouldBeVisible());
			_dimC.setVisible(sequenceIsVisible && _dimC.shouldBeVisible());
		}
		
		/**
		 * Return the selectable values for the input type combo
		 */
		private static Variant.Type[] makeAvailableTypes()
		{
			return new Variant.Type[] {
				Variant.Type.SCALAR  ,
				Variant.Type.ARRAY   ,
				Variant.Type.SEQUENCE
			};
		}
	}
	
	/**
	 * Selector for sub-sequences index ranges (EzVar)
	 */
	private static class EzVarDimensionsPicker extends EzVar<IndexRange>
	{
		private DimensionId   _dimension;
		private EzVarSequence _sequence ;
		
		/**
		 * Constructor
		 */
		public EzVarDimensionsPicker(String name, DimensionId dimension, EzVarSequence sequence)
		{
			super(new VarDimensionsPicker(name, dimension), null);
			_dimension = dimension;
			_sequence  = sequence ;
		}
		
		/**
		 * Whether the component should be visible or not
		 */
		private boolean shouldBeVisible()
		{
			Sequence sequence = _sequence.getValue();
			return sequence!=null && sequence.getSize(_dimension)>=2;
		}
		
		/**
		 * Return a valid and non-empty sub-range
		 */
		public int[] getValidatedIndexRange()
		{
			if(!shouldBeVisible()) {
				return null;
			}
			IndexRange value = getValue();
			if(!value.valid) {
				throw new IcyHandledException("The selected index range for " + name + " is not valid.");
			}
			if(value.subrange!=null && value.subrange.length==0) {
				throw new IcyHandledException("The selected index range for " + name + " is empty.");
			}
			return value.subrange;
		}
	}
	
	/**
	 * Selector for sub-sequences index ranges (Var)
	 */
	private static class VarDimensionsPicker extends Var<IndexRange>
	{
		/**
		 * Editor
		 */
		private class Editor extends SwingVarEditor<IndexRange>
		{
			private DimensionsPickerComponent _component;
			private boolean _shuntVariableUpdate ;
			private boolean _shuntComponentUpdate;
			
			public Editor(VarDimensionsPicker variable)
			{
				super(variable);
				_shuntVariableUpdate  = false;
				_shuntComponentUpdate = false;
			}
			
			@Override
			protected JComponent createEditorComponent()
			{
				_component = new DimensionsPickerComponent(_dimension);
				_component.addValueListener(new DimensionsPickerComponent.ValueListener()
				{
					@Override
					public void valueChanged(boolean valid, int[] newValue)
					{
						if(_shuntVariableUpdate) {
							return;
						}
						_shuntComponentUpdate = true;
						variable.setValue(new IndexRange(valid, newValue));
						_shuntComponentUpdate = false;
					}
				});
				return _component;
			}
	
			@Override
			protected void activateListeners()
			{
				_shuntVariableUpdate = false;
			}
			
			@Override
			protected void deactivateListeners()
			{
				_shuntVariableUpdate = true;
			}
			
			@Override
			protected void updateInterfaceValue()
			{
				if(_shuntComponentUpdate) {
					return;
				}
				IndexRange value = variable.getValue();
				if(value.valid) {
					_component.setValue(value.subrange);
				}
			}
		}
		
		private DimensionId _dimension;
		
		public VarDimensionsPicker(String name, DimensionId dimension)
		{
			super(name, IndexRange.class, new IndexRange());
		}
		
		@Override
		public VarEditor<IndexRange> createVarEditor()
		{
			return new Editor(this);
		}
	}
	
	/**
	 * Pair boolean + int[]
	 */
	private static class IndexRange
	{
		public boolean valid   ;
		public int[]   subrange;
		
		public IndexRange()
		{
			valid    = true;
			subrange = null;
		}
		
		public IndexRange(boolean v, int[] sr)
		{
			valid    = v ;
			subrange = sr;
		}
	}
}
