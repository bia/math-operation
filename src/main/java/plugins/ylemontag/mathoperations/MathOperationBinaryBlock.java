package plugins.ylemontag.mathoperations;

import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.util.VarException;
import plugins.ylemontag.mathoperations.operations.Operation2;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Block for binary operations
 */
public class MathOperationBinaryBlock extends MathOperationAbstractBlock
{
	private VarEnum<Operation2> _operation;
	private String              _name1    ;
	private String              _name2    ;
	
	public MathOperationBinaryBlock()
	{
		super("result");
		_operation = new VarEnum<Operation2>("op", Operation2.ADD);
		_name1     = "a";
		_name2     = "b";
	}
	
	@Override
	public void declareInput(VarList inputMap)
	{
		inputMap.add("Operation", _operation);
		addInputVariant(inputMap, _name1, "In 1");
		addInputVariant(inputMap, _name2, "In 2");
	}
	
	@Override
	public String getMainPluginClassName()
	{
		return MathOperationPlugin.class.getName();
	}
	
	@Override
	public String getName()
	{
		return "Math operation (2)";
	}
	
	@Override
	public void run()
	{
		// Retrieve the operation
		Operation2 op = _operation.getValue();
		if(op==null) {
			throw new VarException("No operation selected");
		}
		
		// Retrieve the input values
		Variant in1 = retrieveInputValue(_name1);
		Variant in2 = retrieveInputValue(_name2);
		String description = op.getFunctor().describeOperation(
			in1.getRepresentation(true), in2.getRepresentation(true));
		
		// Execute the operation and save the value
		try {
			Variant out = op.getFunctor().apply(in1, in2);
			defineOutputValue(out);
		}
		catch(Functor.InconsistentArguments err) {
			reportError(err, description);
		}
	}
}
