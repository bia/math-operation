package plugins.ylemontag.mathoperations.operations;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.*;
import plugins.ylemontag.mathoperations.Functor.FunPtr;
import plugins.ylemontag.mathoperations.functors.Functor1;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Basic operation with 1 argument
 */
public enum Operation1 implements Operation
{
	ABS  ("Absolute value"    , "abs(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.abs   (in[0]); } }),
	SIGN ("Sign"              , "sign(%s)" , new FunPtr() { public double apply(double[] in) { return Math.signum(in[0]); } }),
	SQRT ("Square root"       , "sqrt(%s)" , new FunPtr() { public double apply(double[] in) { return Math.sqrt  (in[0]); } }),
	EXP  ("Exponent"          , "exp(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.exp   (in[0]); } }),
	LOG  ("Natural logarithm" , "log(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.log   (in[0]); } }),
	LOG10("Base 10 logarithm" , "log10(%s)", new FunPtr() { public double apply(double[] in) { return Math.log10 (in[0]); } }),
	COS  ("Cosine"            , "cos(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.cos   (in[0]); } }),
	SIN  ("Sine"              , "sin(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.sin   (in[0]); } }),
	TAN  ("Tangent"           , "tan(%s)"  , new FunPtr() { public double apply(double[] in) { return Math.tan   (in[0]); } }),
	ACOS ("Arc cosine"        , "acos(%s)" , new FunPtr() { public double apply(double[] in) { return Math.acos  (in[0]); } }),
	ASIN ("Arc sine"          , "asin(%s)" , new FunPtr() { public double apply(double[] in) { return Math.asin  (in[0]); } }),
	ATAN ("Arc tangent"       , "atan(%s)" , new FunPtr() { public double apply(double[] in) { return Math.atan  (in[0]); } }),
	COSH ("Hyperbolic cosine" , "cosh(%s)" , new FunPtr() { public double apply(double[] in) { return Math.cosh  (in[0]); } }),
	SINH ("Hyperbolic sine"   , "sinh(%s)" , new FunPtr() { public double apply(double[] in) { return Math.sinh  (in[0]); } }),
	TANH ("Hyperbolic tangent", "tanh(%s)" , new FunPtr() { public double apply(double[] in) { return Math.tanh  (in[0]); } }),
	ROUND("Round"             , "round(%s)", new FunPtr() { public double apply(double[] in) { return Math.round (in[0]); } }),
	FLOOR("Floor"             , "floor(%s)", new FunPtr() { public double apply(double[] in) { return Math.floor (in[0]); } }),
	CEIL ("Ceiling"           , "ceil(%s)" , new FunPtr() { public double apply(double[] in) { return Math.ceil  (in[0]); } }),
	COPY ("Copy"              , "copy(%s)" , new FunPtr() { public double apply(double[] in) { return             in[0] ; } });
	
	private String   _functionName;
	private Functor1 _functor     ;
	
	private Operation1(String functionName, String describePattern, FunPtr functionPointer)
	{
		_functionName = functionName;
		_functor      = new Functor1(describePattern, functionPointer);
	}
	
	@Override
	public String toString()
	{
		return _functionName;
	}
	
	@Override
	public String getFunctionName()
	{
		return _functionName;
	}
	
	@Override
	public Functor getRawFunctor()
	{
		return _functor;
	}
	
	/**
	 * Return the associated functor
	 */
	public Functor1 getFunctor()
	{
		return _functor;
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 */
	public String describeOperation(String in1)
	{
		return _functor.describeOperation(in1);
	}
	
	/*############################################################################
	  # BELOW: auto-generated functions                                        #
	############################################################################*/
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant in1)
	 */
	public double apply(double in1)
	{
		return _functor.apply(in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant in1)
	 */
	public double[] apply(double[] in1)
	{
		return _functor.apply(in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant in1)
	 */
	public Sequence apply(Sequence in1)
	{
		return _functor.apply(in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant in1)
	 */
	public Sequence apply(SubSequence in1)
	{
		return _functor.apply(in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant)
	 */
	public void apply(double[] out, double[] in1)
	{
		_functor.apply(out, in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant)
	 */
	public void apply(Sequence out, Sequence in1)
	{
		_functor.apply(out, in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant)
	 */
	public void apply(Sequence out, SubSequence in1)
	{
		_functor.apply(out, in1);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Controller) 
	 */
	public double[] apply(double[] in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Controller) 
	 */
	public Sequence apply(Sequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Controller)
	 */
	public Sequence apply(SubSequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant, Controller)
	 */
	public void apply(double[] out, double[] in1, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant, Controller)
	 */
	public void apply(Sequence out, Sequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor1#apply(Variant, Variant, Controller)
	 */
	public void apply(Sequence out, SubSequence in1, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, controller);
	}
}
