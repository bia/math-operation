package plugins.ylemontag.mathoperations.operations;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.*;
import plugins.ylemontag.mathoperations.Functor.FunPtr;
import plugins.ylemontag.mathoperations.functors.Functor2;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Basic binary operation
 */
public enum Operation2 implements Operation
{
	ADD     ("Add"     , "%s + %s"   , new FunPtr() { public double apply(double[] in) { return in[0] + in[1]; } }),
	SUBTRACT("Subtract", "%s - %s"   , new FunPtr() { public double apply(double[] in) { return in[0] - in[1]; } }),
	MULTIPLY("Multiply", "%s\u00d7%s", new FunPtr() { public double apply(double[] in) { return in[0] * in[1]; } }),
	DIVIDE  ("Divide"  , "%s\u00f7%s", new FunPtr() { public double apply(double[] in) { return in[0] / in[1]; } }),
	POWER   ("Power"   , "%s^%s"     , new FunPtr() { public double apply(double[] in) { return Math.pow(in[0], in[1]); } }),
	MAX     ("Max"     , "max(%s,%s)", new FunPtr() { public double apply(double[] in) { return Math.max(in[0], in[1]); } }),
	MIN     ("Min"     , "min(%s,%s)", new FunPtr() { public double apply(double[] in) { return Math.min(in[0], in[1]); } });
	
	private String   _functionName;
	private Functor2 _functor     ;
	
	private Operation2(String functionName, String describePattern, FunPtr functionPointer)
	{
		_functionName = functionName;
		_functor      = new Functor2(describePattern, functionPointer);
	}
	
	@Override
	public String toString()
	{
		return _functionName;
	}
	
	@Override
	public String getFunctionName()
	{
		return _functionName;
	}
	
	@Override
	public Functor getRawFunctor()
	{
		return _functor;
	}
	
	/**
	 * Return the associated functor
	 */
	public Functor2 getFunctor()
	{
		return _functor;
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 */
	public String describeOperation(String in1, String in2)
	{
		return _functor.describeOperation(in1, in2);
	}
	
	/*############################################################################
	  # BELOW: auto-generated functions                                        #
	############################################################################*/
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public double apply(double in1, double in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public double[] apply(double in1, double[] in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public double[] apply(double[] in1, double in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public double[] apply(double[] in1, double[] in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(double in1, Sequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(double in1, SubSequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, double in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, Sequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(Sequence in1, SubSequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, double in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, Sequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2)
	{
		return _functor.apply(in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double in1, double[] in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double[] in1, double in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(double[] out, double[] in1, double[] in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, double in1, Sequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, double in1, SubSequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, double in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, double in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2)
	{
		_functor.apply(out, in1, in2);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double[] in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public double[] apply(double[] in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(double in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(double in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(Sequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant in1, Variant in2, Controller controller)
	 */
	public Sequence apply(SubSequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		return _functor.apply(in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(double[] out, double[] in1, double[] in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, double in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, double in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, Sequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant, Variant, Variant, Controller)
	 */
	public void apply(Sequence out, SubSequence in1, double in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, Sequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
	
	/**
	 * Specialized evaluation function
	 * @see Functor2#apply(Variant out, Variant in1, Variant in2, Controller controller)
	 */
	public void apply(Sequence out, SubSequence in1, SubSequence in2, Controller controller)
		throws Controller.CanceledByUser
	{
		_functor.apply(out, in1, in2, controller);
	}
}
