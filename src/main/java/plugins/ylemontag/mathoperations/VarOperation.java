package plugins.ylemontag.mathoperations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.ylemontag.mathoperations.operations.Operation1;
import plugins.ylemontag.mathoperations.operations.Operation2;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Either a Operation1 or a Operation2
 */
public class VarOperation extends Var<Operation>
{
	/**
	 * Editor
	 */
	private static class Editor extends SwingVarEditor<Operation>
	{
		private JComboBox _component;
		private DefaultComboBoxModel _model;
		private boolean _shuntListeners;
		
		public Editor(Var<Operation> variable)
		{
			super(variable);
			_shuntListeners = false;
		}
		
		/**
		 * Feed the model
		 */
		private void feedModel()
		{
			for(Operation op : Operation2.values()) {
				_model.addElement(op);
			}
			for(Operation op : Operation1.values()) {
				_model.addElement(op);
			}
		}

		@Override
		protected JComponent createEditorComponent()
		{
			_model = new DefaultComboBoxModel();
			_component = new JComboBox();
			_component.setModel(_model);
			feedModel();
			_component.addActionListener(new ActionListener()
			{	
				@Override
				public void actionPerformed(ActionEvent e) {
					if(_shuntListeners) {
						return;
					}
					variable.setValue((Operation)_component.getSelectedItem());
				}
			});
			return _component;
		}

		@Override
		protected void activateListeners()
		{
			_shuntListeners = false;
		}

		@Override
		protected void deactivateListeners()
		{
			_shuntListeners = true;
		}

		@Override
		protected void updateInterfaceValue()
		{
			Operation currentValue = variable.getValue();
			if(currentValue!=null) {
				_component.setSelectedItem(currentValue);
			}
		}
	}
	
	public VarOperation(String name, Operation defaultValue)
	{
		super(name, Operation.class, defaultValue);
	}
	
	@Override
	public VarEditor<Operation> createVarEditor()
	{
		return new Editor(this);
	}
	
	@Override
	public String getValueAsString()
	{
		Operation op = getValue();
		if(op!=null) {
			if(op instanceof Operation1) return ((Operation1)op).name();
			if(op instanceof Operation2) return ((Operation2)op).name();
		}
		return "null";
	}
	
	@Override
	public Operation parse(String s)
	{
		try { return Enum.valueOf(Operation1.class, s); } catch(IllegalArgumentException err) {}
		try { return Enum.valueOf(Operation2.class, s); } catch(IllegalArgumentException err) {}
		return null;
	}
}
