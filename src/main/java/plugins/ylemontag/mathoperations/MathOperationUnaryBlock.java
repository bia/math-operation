package plugins.ylemontag.mathoperations;

import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.util.VarException;
import plugins.ylemontag.mathoperations.operations.Operation1;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Block for unary operations
 */
public class MathOperationUnaryBlock extends MathOperationAbstractBlock
{
	private VarEnum<Operation1> _operation;
	private String              _name0    ;
	
	public MathOperationUnaryBlock()
	{
		super("result");
		_operation = new VarEnum<Operation1>("op", Operation1.ABS);
		_name0     = "a";
	}
	
	@Override
	public void declareInput(VarList inputMap)
	{
		inputMap.add("Operation", _operation);
		addInputVariant(inputMap, _name0, "In");
	}
	
	@Override
	public String getMainPluginClassName()
	{
		return MathOperationPlugin.class.getName();
	}
	
	@Override
	public String getName()
	{
		return "Math operation (1)";
	}
	
	@Override
	public void run()
	{
		// Retrieve the operation
		Operation1 op = _operation.getValue();
		if(op==null) {
			throw new VarException("No operation selected");
		}
		
		// Retrieve the input value
		Variant in0 = retrieveInputValue(_name0);
		String description = op.getFunctor().describeOperation(in0.getRepresentation(true));
		
		// Execute the operation and save the value
		try {
			Variant out = op.getFunctor().apply(in0);
			defineOutputValue(out);
		}
		catch(Functor.InconsistentArguments err) {
			reportError(err, description);
		}
	}
}
