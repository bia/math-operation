package plugins.ylemontag.mathoperations;

import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.system.thread.ThreadUtil;

import java.util.LinkedList;

import plugins.ylemontag.mathoperations.variants.DimensionScalar;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a math function into a java object
 * 
 * This class translates the concept of a math function into Java. It can
 * represent either simple math functions, such as (a,b) -> a + b (this 
 * function is named addition!), or more complex ones, such as
 * (a,b,c) -> a + log(b*cos(c)) / 2 (this function does not have any name!).
 * 
 * As various functors may have various number of input arguments, the low-level
 * evaluation methods 'apply' defined in this class take an array of input
 * arguments (usually named 'inputs'), whose length should correspond to the
 * number of arguments expected by the Functor object on which they are called.
 * See the method 'getInputArgumentCount()' to retrieve the number of expected
 * input arguments.
 * 
 * The derived classes Functor1, Functor2, etc... provide more convenient
 * 'apply' methods to use with math functions having respectively 1 argument,
 * 2 arguments or more. Final users should use these classes instead of Functor.
 */
public class Functor
{
    /**
     * Exception thrown when a functor is called with an unsupported combination of arguments
     */
    public static class UnsupportedArguments extends IllegalArgumentException
    {
        private static final long serialVersionUID = 1L;
        
        public UnsupportedArguments() { super(); }
        public UnsupportedArguments(String message) { super(message); }
    }
	/**
	 * Exception thrown when a functor is called with an illegal combination of arguments
	 */
	public static class InconsistentArguments extends IllegalArgumentException
	{
		private static final long serialVersionUID = 1L;
		
		public InconsistentArguments() { super(); }
		public InconsistentArguments(String message) { super(message); }
	}
	
	/**
	 * Exception thrown when a functor is called with a wrong number of arguments
	 */
	public static class WrongArgumentNumber extends IllegalArgumentException
	{
		private static final long serialVersionUID = 1L;
		
		public WrongArgumentNumber() { super(); }
		public WrongArgumentNumber(String message) { super(message); }
	}
	
	/**
	 * Try to interpret the string 'expr' as a function of the variables 'variables'
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor rawParse(String expr, String[] variables)
	{
		return Expression.parse(expr).getFunctor(variables);
	}
	
	/**
	 * Try to interpret the string 'expr' as a function whose input variables are auto-detected
	 * @throws IllegalArgumentException If 'expr' is not a well-formed expression. 
	 */
	public static Functor rawParse(String expr)
	{
		return Expression.parse(expr).getFunctor();
	}
	
	/**
	 * Determine whether a given list of inputs could be aggregated in an operation
	 */
	public static boolean isConsistentInputTypes(Variant.Type[] inputTypes)
	{
		try {
			computeOutputType(inputTypes);
			return true;
		}
		catch(InconsistentArguments err) {
			return false;
		}
	}
	
	/**
	 * Determine the output type given the types of the input arguments
	 * @throws InconsistentArguments if the combination of input types is
	 *         incoherent (ex: array together with sequence).
	 */
	public static Variant.DimensionType computeOutputType(Variant.Type[] inputTypes)
	{
		Variant.DimensionType retVal = Variant.DimensionType.SCALAR;
		for(Variant.Type t : inputTypes)
		{
			// Null input type values are not taken into consideration
			if(t==null) {
				continue;
			}
			
			// Regular cases
			switch(t.getDimensionType())
			{
				// Input scalar: the output type does not change
				case SCALAR:
					break;
					
				// Input array: the output type change for array if it is set to scalar
				case ARRAY:
					if(retVal==Variant.DimensionType.SCALAR || retVal==Variant.DimensionType.ARRAY) {
						retVal = Variant.DimensionType.ARRAY;
					}
					else {
						throw new InconsistentArguments(
							"Cannot operate on a array and on a " + retVal.toString().toLowerCase() + " at the same time."
						);
					}
					break;
					
				// Input sequence: the output type change for sequence if it is set to scalar
				case SEQUENCE:
					if(retVal==Variant.DimensionType.SCALAR || retVal==Variant.DimensionType.SEQUENCE) {
						retVal = Variant.DimensionType.SEQUENCE;
					}
					else {
						throw new InconsistentArguments(
							"Cannot operate on a sequence and on a " + retVal.toString().toLowerCase() + " at the same time."
						);
					}
					break;
				
				// Unreachable point
				default:
					throw new RuntimeException("Unreachable code point");
			}
		}
		return retVal;
	}
	
	/**
	 * Base object used to represent math functions (would be a function pointer in C)
	 */
	public interface FunPtr
	{
		public double apply(double[] in);
	}
	
	private int    _inputArguments ;
	private String _describePattern;
	private FunPtr _functionPointer;
	
	/**
	 * Constructor
	 */
	public Functor(int inputArguments, String describePattern, FunPtr functionPointer)
	{
		_inputArguments  = inputArguments ;
		_describePattern = describePattern;
		_functionPointer = functionPointer;
	}
	
	/**
	 * Return the number of arguments that are expected by the functor
	 */
	public int getInputArgumentCount()
	{
		return _inputArguments;
	}
	
	/**
	 * Return the format pattern used to describe the functor
	 */
	public String getFormatPattern()
	{
		return _describePattern;
	}
	
	/**
	 * Return the underlying math function
	 */
	public FunPtr getFunctionPointer()
	{
		return _functionPointer;
	}
	
	/**
	 * Return a string that describe the functor operation applied to the given list of input arguments
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 */
	public String describeOperation(String[] inputs)
	{
		ensureValidNumberOfArguments(inputs.length);
		return String.format(_describePattern, (Object[])inputs);
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 */
	public Variant apply(Variant[] inputs)
	{
		try {
			return apply(inputs, null);
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'output'
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 */
	public void apply(Variant output, Variant[] inputs)
	{
		try {
			apply(output, inputs, null);
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public Variant apply(Variant[] inputs, Controller controller) throws Controller.CanceledByUser
	{
		Variant retVal = allocateOutput(inputs);
		apply(retVal, inputs, controller);
		return retVal;
	}
	
	/**
	 * Execute iteratively the core function on all the samples of the inputs,
	 * and save the result to the variable 'output'
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...),
	 *         or if the output buffer is not writable.
	 * @throws Controller.CanceledByUser If the operation is interrupted through the Controller object.
	 */
	public void apply(Variant output, Variant[] inputs, final Controller controller) throws Controller.CanceledByUser
	{
		// Make sure that we have the correct number of input arguments
		ensureValidNumberOfArguments(inputs.length);
		
		// Make sure that the output is writable, and the inputs readable
		if(!output.isWritable()) {
			throw new InconsistentArguments("The allocated output object is not writable.");
		}
		for(Variant in : inputs) {
			if(!in.isReadable()) {
				throw new InconsistentArguments("One of the input arguments is not readable.");
			}
		}
		
		// Make sure that the output is properly allocated, and that the size of the
		// inputs are self-consistent and consistent with the size of the output.
		Variant.Dimension outputDim   = output.getDimension();
		Variant.Dimension thOutputDim = computeOutputDimension(inputs);
		if(!outputDim.equals(thOutputDim)) {
			throw new InconsistentArguments(
				"The output does not have the right dimensions (expected: " + thOutputDim + "; current: " + outputDim + ")."
			);
		}
		
		// Size, granularity and number of blocks
		long sampleCount = outputDim.getFlatSize();
		int granularity = outputDim.getGranularity();
		if(granularity==0 || (sampleCount % granularity != 0)) {
			throw new IllegalStateException("The granularity must be a divisor of the flat size.");
		}
		
		// Schedule the jobs
		LinkedList<Job> jobs = new LinkedList<Job>();
		int maximumNumberOfJobs  = SystemUtil.getAvailableProcessors();
		int minimumSamplesPerJob = 1024*1024;
		int minimumBlocksPerJob  = minimumSamplesPerJob / granularity;
		long startAt              = 0;
		long remainingBlocks     = sampleCount / granularity;
		while(remainingBlocks>0)
		{
			// Number of remaining available jobs
			int remainingJobs = maximumNumberOfJobs - jobs.size();
			if(remainingJobs<=0) {
				throw new RuntimeException("Unreachable code point");
			}
			
			// Determine the number of blocks that should be processed by the next job,
			// which is approximately the number of remaining blocks divided by the
			// number of remaining jobs.
			long currentNumberOfBlocks = (remainingBlocks + remainingJobs - 1)/remainingJobs;
			// Equivalent to currentNumberOfBlocks = Math.ceil( (double)remainingBlocks / (double)remainingJobs );
			
			// Adjust the number of blocks: it should be greater than the threshold
			// 'minimumBlocksPerJob' (to avoid the situation where a large number of
			// jobs is allocated while each of them process only a small number of
			// samples). However, the number of blocks allocated to the next job must
			// remain lower than the remaining number of blocks.
			currentNumberOfBlocks = Math.max(currentNumberOfBlocks, minimumBlocksPerJob);
			currentNumberOfBlocks = Math.min(currentNumberOfBlocks, remainingBlocks    );
			
			// Allocate the new job, and update the counters
			long length = currentNumberOfBlocks*granularity;
			jobs.addLast(new Job(output, inputs, controller, startAt, length));
			startAt         += length;
			remainingBlocks -= currentNumberOfBlocks;
		}
		
		// Feed the controller with the job probes
		try {
			if(controller!=null) {
				controller.clearProbes();
				for(Job job : jobs) {
					controller.pushProbe(job.getProbe());
				}
			}
			
			// Run the jobs
			output.beginUpdate();
			try {
				if(jobs.size()==0) {
					// Nothing to do
				}
				else if(jobs.size()==1) {
					jobs.getFirst().doTheJob();
				}
				else {
					Processor processor = new Processor(jobs.size(), SystemUtil.getAvailableProcessors());
					for(Job job : jobs) {
						processor.addTask(job);
					}
					ThreadUtil.sleep(50);
					processor.waitAll();
					if(controller!=null) {
						controller.checkPoint();
					}
				}
			}
			finally {
				output.endUpdate();
			}
		}
		
		// Clear the probes
		finally {
			if(controller!=null) {
				controller.clearProbes();
			}
		}
	}
	
	/**
	 * Sub-task
	 */
	private class Job implements Runnable
	{
		private Variant          _out       ;
		private Variant[]        _in        ;
		private final Controller _controller;
		long                     _startAt   ;
		long                     _length    ;
		int                      _counter   ;
		
		/**
		 * Constructor
		 */
		public Job(Variant out, Variant[] in, Controller controller, long startAt, long length)
		{
			_out        = out       ;
			_in         = in        ;
			_controller = controller;
			_startAt    = startAt   ;
			_length     = length    ;
			_counter    = 0         ;
		}
		
		/**
		 * Return the probe associated to this job
		 */
		public Controller.JobProbe getProbe()
		{
			return new Controller.JobProbe()
			{				
				@Override
				public long getMaxStep()
				{
					return _length;
				}
				
				@Override
				public long getCurrentStep()
				{
					return _counter;
				}
			};
		}
		
		/**
		 * Run the job
		 */
		public void doTheJob() throws Controller.CanceledByUser
		{
			if(_controller!=null) {
				_controller.checkPoint();
			}
			double              [] buffer = new double              [_in.length]; 
			Variant.ReadIterator[] itIn   = new Variant.ReadIterator[_in.length];
			
			// Initialize the cursors
			Variant.WriteIterator itOut = _out.getWriteIterator();
			itOut.startAt(_startAt);
			for(int k=0; k<_in.length; ++k) {
				itIn[k] = _in[k].getReadIterator();
				itIn[k].startAt(_startAt);
			}
			
			// Loop over the '_length' samples that are affected to this job
			for(_counter=0; _counter<_length; ++_counter) {
				if(_controller!=null) {
					_controller.checkPoint();
				}
				for(int k=0; k<_in.length; ++k) {
					buffer[k] = itIn[k].get();
					itIn  [k].next();
				}
				itOut.set(_functionPointer.apply(buffer));
				itOut.next();
			}
		}
		
		@Override
		public void run()
		{
			try {
				doTheJob();
			}
			catch(Controller.CanceledByUser err) {}
		}
	}
	
	/**
	 * Throw an exception in the given list of input arguments has not the expected size
	 * @throws WrongArgumentNumber If the number of arguments is different from what is expected.
	 */
	private void ensureValidNumberOfArguments(int inputLength)
	{
		if(inputLength!=_inputArguments) {
			throw new WrongArgumentNumber(
				"Wrong number of input arguments (expected: " + _inputArguments + "; current: " + inputLength + ")."
			);
		}
	}
	
	/**
	 * Allocate the output object
	 * @throws InconsistentArguments If the combination of input arguments is inconsistent
	 *         (ex: array together with sequence, sequences with incompatible sizes, etc...).
	 */
	private Variant allocateOutput(Variant[] inputs)
	{
		Variant retVal = computeOutputDimension(inputs).allocateNewVariant();
		if(retVal.getType()==Variant.Type.SEQUENCE) // Set the name of the newly-allocated sequences
		{
			String[] names = new String[inputs.length];
			for(int k=0; k<inputs.length; ++k) {
				names[k] = inputs[k].getRepresentation(true);
			}
			retVal.getAsSequence().setName(describeOperation(names));
		}
		return retVal;
	}
	
	/**
	 * Determine the size of the output given the input arguments
	 * @throws InconsistentArguments if the combination of input types is incoherent.
	 */
	private static Variant.Dimension computeOutputDimension(Variant[] inputs)
	{
		Variant.Dimension retVal = new DimensionScalar();
		for(Variant in : inputs)
		{
			switch(in.getType().getDimensionType())
			{
				// Input scalar: the output size does not change
				case SCALAR:
					break;
					
				// Input array: the output size change if it is set to scalar
				case ARRAY:
					if(retVal.getType()==Variant.DimensionType.SCALAR) {
						retVal = in.getDimension();
					}
					else if(!retVal.equals(in.getDimension())) {
						throw new InconsistentArguments(
							"Wrong combination of inputs: " + retVal + " together with " + in + "."
						);
					}
					break;
					
				// Input sequence or sub-sequence: the output size change if it is set to scalar
				case SEQUENCE:
					if(retVal.getType()==Variant.DimensionType.SCALAR) {
						retVal = in.getDimension();
					}
					else if(!retVal.equals(in.getDimension())) {
						throw new InconsistentArguments(
							"Wrong combination of inputs: " + retVal + " together with " + in + "."
						);
					}
					break;
				
				// Unreachable point
				default:
					throw new RuntimeException("Unreachable code point");
			}
		}
		return retVal;
	}
}
