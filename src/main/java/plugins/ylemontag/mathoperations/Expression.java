package plugins.ylemontag.mathoperations;

import java.util.SortedSet;

import plugins.ylemontag.mathoperations.expressions.Parser;
import plugins.ylemontag.mathoperations.functors.Functor1;
import plugins.ylemontag.mathoperations.functors.Functor2;
import plugins.ylemontag.mathoperations.functors.Functor3;
import plugins.ylemontag.mathoperations.functors.Functor4;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Algebraic expression
 * 
 * This class provides the tools needed to analyze strings that represent a math
 * expression (ex: "3*x + r*cos(theta)"), and to generate the corresponding
 * functors.
 * 
 * Final users may be interested in the static functions Functor.parse(),
 * Functor1.parse(), Functor2.parse(), etc., that take as first argument
 * a string representing a math expression, and return the corresponding
 * functor (if the expression is well-formed).
 * 
 * See the method Expression.getFunctor(String[] variables) to get more details
 * on how functors are generated from expressions.
 */
public abstract class Expression
{
	/**
	 * Exception thrown by the parsing functions
	 */
	public static class ParsingException extends IllegalArgumentException
	{
		private static final long serialVersionUID = 1L;
		
		public ParsingException() { super(); }
		public ParsingException(String message) { super(message); }
	}
	
	/**
	 * Exception thrown when a expression (even a valid one) cannot be interpreted as a functor
	 */
	public static class BadFunctor extends IllegalArgumentException
	{
		private static final long serialVersionUID = 1L;
		
		public BadFunctor() { super(); }
		public BadFunctor(String message) { super(message); }
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null || !(obj instanceof Expression)) {
			return false;
		}
		else {
			return equals((Expression)obj);
		}
	}
	
	@Override
	public String toString()
	{
		return getRepresentation(true);
	}
	
	/**
	 * @return Recursive function in charge of checking that both expressions are equal
	 * @param expr Expression
	 */
	public abstract boolean equals(Expression expr);
	
	/**
	 * @return a human-readable string representing the expression
	 * @param smartParenthesis boolean
	 */
	public abstract String getRepresentation(boolean smartParenthesis);
	
	/**
	 * @return  the list of the variables mentioned in the expression
	 * 
	 * Example: the variables associated to the expression "a + 2*b/a" are "a" and "b".
	 */
	public abstract SortedSet<String> getVariables();
	
	/**
	 * @return the list of the variables mentioned in the expression as an alphabetically sorted array
	 */
	public String[] getVariablesAsArray()
	{
		SortedSet<String> variables = getVariables();
		String[] retVal = new String[variables.size()];
		int k = 0;
		for(String v : variables) {
			retVal[k] = v;
			++k;
		}
		return retVal;
	}
	
	/**
	 * @return Check if the current expression represents a valid functor for the given list of variables
	 * 
	 * Example: the expression can be interpreted as a valid functor "a + 2*b/a"
	 * given any list that contains at least "a" and "b".
	 * 
	 * This may also return false if some unknown functions are mentioned in the
	 * expression. For instance: "a + foo(b)" (the function foo does not exist).
	 * @param variables array of Strings
	 */
	public abstract boolean isValidFunctor(String[] variables);
	
	/**
	 * Check if the current expression represents a valid functor for its canonical list
	 * of variables
	 */
	public boolean isValidFunctor()
	{
		return isValidFunctor(null);
	}
	
	/**
	 * @return Check if the current expression represents a valid functor with the given input variable
	 * @param v1 String
	 */
	public boolean isValidFunctor1(String v1)
	{
		return isValidFunctor(new String[] { v1 });
	}
	
	/**
	 * @return Check if the current expression represents a valid functor with the given input variables
	 * @param v1 String
	 * @param v2 String
	 */
	public boolean isValidFunctor2(String v1, String v2)
	{
		return isValidFunctor(new String[] { v1, v2 });
	}
	
	/**
	 * @return Check if the current expression represents a valid functor with the given input variables
	 * @param v1 String
	 * @param v2 String
	 * @param v3 String
	 */
	public boolean isValidFunctor3(String v1, String v2, String v3)
	{
		return isValidFunctor(new String[] { v1, v2, v3 });
	}
	
	/**
	 * @return Check if the current expression represents a valid functor with the given input variables
	 * @param v1 String
	 * @param v2 String
	 * @param v3 String
	 * @param v4 String
	 */
	public boolean isValidFunctor4(String v1, String v2, String v3, String v4)
	{
		return isValidFunctor(new String[] { v1, v2, v3, v4 });
	}
	
	/**
	 * @return Return the associated functor
	 * @param variables Array of String
	 * @throws BadFunctor If !isValidFunctor(variables)
	 * 
	 * Please not that the order in which the variables in mentioned matters. For
	 * example, consider the expression "a + 100*b":
	 *  - getFunctor("a", "b") returns a functor f1, and f1(3, 20) will be evaluated as 2003,
	 *  - getFunctor("b", "a") returns a functor f2, and f2(3, 20) will be evaluated as 320.
	 *  
	 * It is also possible to mention other variables than "a" and "b" in the array
	 * 'variables'. For instance, getFunctor("a", "b", "c") applied on the previous
	 * expression "a + 100*b" returns a functor f3, that requires 3 arguments to
	 * be evaluated, although the third one has no influence on the result.
	 * 
	 * However, getFunctor("a") applied on the previous expression "a + 100*b"
	 * will result in an error, as the variable "b" mentioned in the expression
	 * is not binded to any of the input argument defined for the functor.
	 */
	public abstract Functor getFunctor(String[] variables);
	
	/**
	 * @return Return the associated functor for its canonical list of variables
	 * @throws BadFunctor If !isValidFunctor()
	 */
	public Functor getFunctor()
	{
		return getFunctor(getVariablesAsArray());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @param v1 String
	 * @throws BadFunctor If !isValidFunctor1(v1)
	 */
	public Functor1 getFunctor1(String v1)
	{
		Functor fun = getFunctor(new String[] { v1 });
		return new Functor1(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @throws BadFunctor If !isValidFunctor() || getVariable().size()!=1
	 */
	public Functor1 getFunctor1()
	{
		Functor fun = getCanonicalFunctor(1);
		return new Functor1(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @param v1 String
	 * @param v2 String
	 * @throws BadFunctor If !isValidFunctor2(v1, v2)
	 */
	public Functor2 getFunctor2(String v1, String v2)
	{
		Functor fun = getFunctor(new String[] { v1, v2 });
		return new Functor2(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @throws BadFunctor If !isValidFunctor() || getVariable().size()!=2
	 */
	public Functor2 getFunctor2()
	{
		Functor fun = getCanonicalFunctor(2);
		return new Functor2(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @param v1 String
	 * @param v2 String
	 * @param v3 String
	 * @throws BadFunctor If !isValidFunctor3(v1, v2, v3)
	 */
	public Functor3 getFunctor3(String v1, String v2, String v3)
	{
		Functor fun = getFunctor(new String[] { v1, v2, v3 });
		return new Functor3(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @throws BadFunctor If !isValidFunctor() || getVariable().size()!=3
	 */
	public Functor3 getFunctor3()
	{
		Functor fun = getCanonicalFunctor(3);
		return new Functor3(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @param v1 String
	 * @param v2 String
	 * @param v3 String
	 * @param v4 String
	 * @throws BadFunctor If !isValidFunctor4(v1, v2, v3, v4)
	 */
	public Functor4 getFunctor4(String v1, String v2, String v3, String v4)
	{
		Functor fun = getFunctor(new String[] { v1, v2, v3, v4 });
		return new Functor4(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Return the associated functor for the given input variable
	 * @throws BadFunctor If !isValidFunctor() || getVariable().size()!=4
	 */
	public Functor4 getFunctor4()
	{
		Functor fun = getCanonicalFunctor(4);
		return new Functor4(fun.getFormatPattern(), fun.getFunctionPointer());
	}
	
	/**
	 * @return Ensure that the number of canonical variables is equal to the expected number,
	 * and return the canonical functor
	 * @param expectedInputArgumentCount int
	 * @throws BadFunctor If !isValidFunctor()
	 * @throws BadFunctor If getVariable().size()!=expectedInputArgumentCount
	 */
	private Functor getCanonicalFunctor(int expectedInputArgumentCount)
	{
		String[] canonicalVariables = getVariablesAsArray();
		if(canonicalVariables.length!=expectedInputArgumentCount) {
			throw new BadFunctor(String.format(
				"Wrong number of input variables (expected: %d, current: %d).",
				expectedInputArgumentCount, canonicalVariables.length 
			));
		}
		return getFunctor(canonicalVariables);
	}
	
	/**
	 * @return Parsing function
	 * @param expr String
	 * @throws ParsingException In the given string does not represent a well-formed expression.
	 */
	public static Expression parse(String expr)
	{
		return Parser.parse(expr);
	}
}
