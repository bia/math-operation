package plugins.ylemontag.mathoperations.expressions;

import java.util.SortedSet;
import java.util.TreeSet;

import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Terminal node in an Math expression syntaxic tree, representing a free variable
 * 
 * End users should not use this class.
 */
public class ExpressionVariable extends Expression
{
	private String _name;
	
	/**
	 * Constructor
	 */
	public ExpressionVariable(String name)
	{
		super();
		_name = name;
	}
	
	/**
	 * Name of the variable
	 */
	public String getName()
	{
		return _name;
	}
	
	@Override
	public boolean equals(Expression expr)
	{
		return (expr instanceof ExpressionVariable) && _name.equals(((ExpressionVariable)expr)._name);
	}
	
	@Override
	public String getRepresentation(boolean smartParenthesis)
	{
		return _name;
	}
	
	@Override
	public SortedSet<String> getVariables()
	{
		SortedSet<String> retVal = new TreeSet<String>();
		retVal.add(_name);
		return retVal;
	}
	
	@Override
	public boolean isValidFunctor(String[] variables)
	{
		if(variables==null) {
			return true;
		}
		else {
			for(String v : variables) {
				if(_name.equals(v)) {
					return true;
				}
			}
			return false;
		}
	}
	
	@Override
	public Functor getFunctor(String[] variables)
	{
		final int index   = searchVariableIndex(variables);
		String    pattern = "%" + Integer.toString(1+index) + "$s";
		return new Functor(variables.length, pattern, new Functor.FunPtr()
		{
			@Override
			public double apply(double[] in)
			{
				return in[index];
			}
		});
	}
	
	/**
	 * Search for the index of the current variable in the given table of variable
	 */
	private int searchVariableIndex(String[] variables)
	{
		for(int k=0; k<variables.length; ++k) {
			if(_name.equals(variables[k])) {
				return k;
			}
		}
		throw new BadFunctor("Cannot find '" + _name + "' in the variable table.");
	}
}
