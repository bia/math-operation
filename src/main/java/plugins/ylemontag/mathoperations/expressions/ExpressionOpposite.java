package plugins.ylemontag.mathoperations.expressions;

import java.util.SortedSet;

import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Node of Math expression syntaxic tree that represent a unary minus sign
 * 
 * End users should not use this class.
 */
public class ExpressionOpposite extends Expression
{
	private Expression _rhs;
	
	/**
	 * Constructor
	 */
	public ExpressionOpposite(Expression rhs)
	{
		super();
		_rhs = rhs;
	}
	
	/**
	 * Right-hand side of the minus sign
	 */
	public Expression getRhs()
	{
		return _rhs;
	}
	
	@Override
	public boolean equals(Expression expr)
	{
		return (expr instanceof ExpressionOpposite) && _rhs.equals(((ExpressionOpposite)expr)._rhs);
	}
	
	@Override
	public String getRepresentation(boolean smartParenthesis)
	{
		return "-" + addParenthesisIfNecessary(_rhs.getRepresentation(smartParenthesis), smartParenthesis);
	}
	
	/**
	 * Add parenthesis to the representation of the given expression if they are
	 * necessary to ensure operator priority consistency
	 */
	private String addParenthesisIfNecessary(String in, boolean smartParenthesis)
	{
		String retVal = in;
		if(_rhs instanceof ExpressionOpposite) {
			retVal = "(" + retVal + ")";
		}
		else if(_rhs instanceof ExpressionComposition) {
			int exprPriority = ((ExpressionComposition)_rhs).getOperator().getPriority();
			if(!smartParenthesis || exprPriority==0) {
				retVal = "(" + retVal + ")";
			}
		}
		return retVal;
	}
	
	@Override
	public SortedSet<String> getVariables()
	{
		return _rhs.getVariables();
	}
	
	@Override
	public boolean isValidFunctor(String[] variables)
	{
		return _rhs.isValidFunctor(variables);
	}
	
	@Override
	public Functor getFunctor(String[] variables)
	{
		Functor functor = _rhs.getFunctor(variables);
		String  pattern = "-" + addParenthesisIfNecessary(functor.getFormatPattern(), true);
		final Functor.FunPtr funPtr = functor.getFunctionPointer();
		return new Functor(variables.length, pattern, new Functor.FunPtr()
		{
			@Override
			public double apply(double[] in)
			{
				return - funPtr.apply(in);
			}
		});
	}
}
