package plugins.ylemontag.mathoperations.expressions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Node in a Math expression syntax tree that represents a call to a named function
 */
public class ExpressionFunction extends Expression
{
	private String       _function ;
	private Expression[] _arguments;
	
	/**
	 * Constructor
	 */
	public ExpressionFunction(String function, List<Expression> arguments)
	{
		super();
		_function  = function;
		_arguments = new Expression[arguments.size()];
		int k = 0;
		for(Expression e : arguments) {
			_arguments[k] = e;
			++k;
		}
	}
	
	/**
	 * Function name
	 */
	public String getFunction()
	{
		return _function;
	}
	
	/**
	 * Return the number of arguments
	 */
	public int getArgumentCount()
	{
		return _arguments.length;
	}
	
	/**
	 * Return the k^th argument
	 */
	public Expression getArgument(int k)
	{
		return _arguments[k];
	}
	
	@Override
	public boolean equals(Expression expr)
	{
		if(!(expr instanceof ExpressionFunction)) {
			return false;
		}
		ExpressionFunction e = (ExpressionFunction)expr;
		if(!_function.equals(e._function) || _arguments.length!=e._arguments.length) {
			return false;
		}
		for(int k=0; k<_arguments.length; ++k) {
			if(!_arguments[k].equals(e._arguments[k])) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String getRepresentation(boolean smartParenthesis)
	{
		String retVal = _function + "(";
		for(int k=0; k<_arguments.length; ++k) {
			if(k!=0) {
				retVal += ",";
			}
			retVal += _arguments[k].getRepresentation(smartParenthesis);
		}
		retVal += ")";
		return retVal;
	}
	
	@Override
	public SortedSet<String> getVariables()
	{
		SortedSet<String> retVal = new TreeSet<String>();
		for(int k=0; k<_arguments.length; ++k) {
			retVal.addAll(_arguments[k].getVariables());
		}
		return retVal;
	}
	
	@Override
	public boolean isValidFunctor(String[] variables)
	{
		if(!isCurrentFunctionKnown()) {
			return false;
		}
		for(int k=0; k<_arguments.length; ++k) {
			if(!_arguments[k].isValidFunctor(variables)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check if the current function name refers to an existing function, 
	 * and if it is called with the right number of arguments
	 */
	private boolean isCurrentFunctionKnown()
	{
		createFunctionIndex();
		Integer expectedArguments = _functionIndex.get(_function);
		return expectedArguments!=null && expectedArguments==_arguments.length;
	}
	
	/**
	 * Create a function index to speed-up the search
	 * 
	 * The function contains the name (as they should be refered to in expressions)
	 * of all the available functions, associated to the corresponding expected
	 * number of arguments of the function.
	 */
	private static void createFunctionIndex()
	{
		if(_functionIndex!=null) {
			return;
		}
		Map<String, Integer> retVal = new HashMap<String, Integer>();
		retVal.put("max"  , 2);
		retVal.put("min"  , 2);
		retVal.put("abs"  , 1);
		retVal.put("sign" , 1);
		retVal.put("sqrt" , 1);
		retVal.put("exp"  , 1);
		retVal.put("log"  , 1);
		retVal.put("log10", 1);
		retVal.put("cos"  , 1);
		retVal.put("sin"  , 1);
		retVal.put("tan"  , 1);
		retVal.put("acos" , 1);
		retVal.put("asin" , 1);
		retVal.put("atan" , 1);
		retVal.put("cosh" , 1);
		retVal.put("sinh" , 1);
		retVal.put("tanh" , 1);
		retVal.put("round", 1);
		retVal.put("floor", 1);
		retVal.put("ceil" , 1);
		_functionIndex = retVal;
	}
	
	/**
	 * Function index
	 */
	private static Map<String, Integer> _functionIndex = null;
	
	@Override
	public Functor getFunctor(String[] variables)
	{
		// Ensure that the current function exists
		if(!isCurrentFunctionKnown()) {
			throw new BadFunctor(String.format(
				"Function '%s' does not exist is called with a wrong number of arguments.", _function
			));
		}
		
		// Retrieve the functors associated to the child expressions, and
		// compose the description pattern
		String pattern = _function + "(";
		final Functor.FunPtr[] funPtr  = new Functor.FunPtr[_arguments.length];
		for(int k=0; k<_arguments.length; ++k) {
			if(k>0) {
				pattern += ",";
			}
			Functor currentFunctor = _arguments[k].getFunctor(variables);
			pattern  += currentFunctor.getFormatPattern  ();
			funPtr[k] = currentFunctor.getFunctionPointer();
		}
		pattern += ")";
		
		// Return the final functors
		if(_function.equals("max")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.max(funPtr[0].apply(in), funPtr[1].apply(in));
				}
			});
		}
		if(_function.equals("min")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.min(funPtr[0].apply(in), funPtr[1].apply(in));
				}
			});
		}
		if(_function.equals("abs")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.abs(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("sign")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.signum(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("sqrt")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.sqrt(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("exp")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.exp(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("log")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.log(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("log10")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.log10(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("cos")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.cos(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("sin")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.sin(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("tan")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.tan(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("acos")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.acos(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("asin")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.asin(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("atan")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.atan(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("cosh")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.cosh(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("sinh")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.sinh(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("tanh")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.tanh(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("round")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.round(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("floor")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.floor(funPtr[0].apply(in));
				}
			});
		}
		if(_function.equals("ceil")) {
			return new Functor(variables.length, pattern, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.ceil(funPtr[0].apply(in));
				}
			});
		}
		
		// The initial call 'isCurrentFunctionKnown()' must ensure that this point
		// in the code is never reached.
		throw new RuntimeException("Unreachable code point");
	}
}
