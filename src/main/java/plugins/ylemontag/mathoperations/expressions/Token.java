package plugins.ylemontag.mathoperations.expressions;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import plugins.ylemontag.mathoperations.Expression;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Atomic token used for Math expression parsing
 * 
 * End users should not use this class.
 */
public class Token
{
	/**
	 * Available types of tokens
	 */
	public static enum Type
	{
		PLUS    ("+" , "\\+"),
		MINUS   ("-" , "-"  ),
		TIMES   ("*" , "\\*"),
		DIV_SIGN("/" , "/"  ),
		POW_SIGN("^" , "\\^"),
		OPEN_P  ("(" , "\\("),
		CLOSE_P (")" , "\\)"),
		COMMA   ("," , ","  ),
		ID      ("ID", "([A-Za-z_][A-Za-z_0-9]*)"),
		LITTERAL("L" , "([0-9]+(?:\\.[0-9]+)?)");
		
		private String  _name   ;
		private Pattern _pattern;
		
		private Type(String name, String regex)
		{
			_name    = name;
			_pattern = Pattern.compile("^[ \\t]*" + regex);
		}
		
		public String getName()
		{
			return _name;
		}
		
		public Pattern getPattern()
		{
			return _pattern;
		}
	}
	
	private Type   _type ;
	private Object _value;
	
	/**
	 * Constructor for tokens with no value
	 */
	private Token(Type type)
	{
		this(type, null);
	}
	
	/**
	 * Constructor
	 */
	private Token(Type type, Object value)
	{
		_type  = type ;
		_value = value;
	}
	
	/**
	 * Return the type of the token
	 */
	public Type getType()
	{
		return _type;
	}
	
	/**
	 * Return the token value if any
	 */
	public Object getValue()
	{
		return _value;
	}
	
	@Override
	public String toString()
	{
		String retVal = _type.getName();
		if(_value!=null) {
			retVal += "(" + _value + ")";
		}
		return retVal;
	}
	
	/**
	 * Parse a list of tokens
	 * @throws Expression.ParsingException If the given string does not represent a valid list of tokens.
	 */
	public static LinkedList<Token> parse(String in)
	{
		LinkedList<Token> retVal = new LinkedList<Token>();
		int pos    = 0;
		int length = in.length();
		while(pos<length)
		{	
			// Try to detect the type of the next token
			Type    type = null;
			Matcher m    = null;
			for(Type t : Type.values()) {
				m = t.getPattern().matcher(in.subSequence(pos, length));
				if(m.find()) {
					type = t;
					break;
				}
			}
			
			// If no token type matches, try to match the end of the stream
			if(type==null) {
				if(!Pattern.matches("[ \\t]*", in.subSequence(pos, length))) {
					throw new Expression.ParsingException("Invalid token stream");
				}
				pos = length; // break
			}
			
			// Otherwise, decode the token, and increment the index
			else {
				pos += m.group(0).length();
				retVal.add(decodeMatchedToken(type, m));
			}
		}
		return retVal;
	}
	
	/**
	 * Decode the matched token
	 */
	private static Token decodeMatchedToken(Type type, Matcher m)
	{
		// Decode the value of the ID
		if(type==Type.ID) {
			if(m.groupCount()!=1) {
				throw new Expression.ParsingException("ID token badly formatted");
			}
			return new Token(type, m.group(1));
		}
		
		// Decode the value of the litteral
		else if(type==Type.LITTERAL) {
			if(m.groupCount()!=1) {
				throw new Expression.ParsingException("Litteral token badly formatted");
			}
			try {
				double value = Double.parseDouble(m.group(1));
				return new Token(type, value);
			}
			catch(NumberFormatException err) {
				throw new Expression.ParsingException("Litteral token badly formatted");
			}
		}
		
		// Otherwise, return a simple token
		else {
			return new Token(type);
		}
	}
}
