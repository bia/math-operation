package plugins.ylemontag.mathoperations.expressions;


/**
 * 
 * @author Yoann Le Montagner
 * 
 * Operators that appear in Math expressions
 * 
 * End users should not use this class.
 */
public enum CompositionOperator
{
	ADD     (0, Token.Type.PLUS    ),
	SUBTRACT(0, Token.Type.MINUS   ),
	MULTIPLY(1, Token.Type.TIMES   ),
	DIVIDE  (1, Token.Type.DIV_SIGN),
	POWER   (2, Token.Type.POW_SIGN);
	
	private int        _priority;
	private Token.Type _token   ;
	
	/**
	 * Constructor
	 */
	private CompositionOperator(int priority, Token.Type token)
	{
		_priority = priority;
		_token    = token   ;
	}
	
	/**
	 * Return the priority level of the operator
	 */
	public int getPriority()
	{
		return _priority;
	}
	
	/**
	 * Return the token type that is used to represent the operator
	 */
	public Token.Type getToken()
	{
		return _token;
	}
}
