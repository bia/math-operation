package plugins.ylemontag.mathoperations.expressions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import plugins.ylemontag.mathoperations.Expression;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Static functions in charge of expression parsing
 * 
 * End users should not use this class.
 */
public class Parser
{
	/**
	 * Parsing function
	 */
	public static Expression parse(String in)
	{
		List<Token> tokens = Token.parse(in);
		return interpretAsExpression(tokens);
	}
	
	/**
	 * Core routine of the parse function
	 */
	private static Expression interpretAsExpression(List<?> in)
	{
		// Interpret and reduce all the expressions bounded by parenthesis
		in = reduceParenthesis(in);
		
		// Convert all the litterals and remaining IDs to expressions
		in = identifyTerminalExpressions(in);
		
		// At this point, there is only expressions and operator tokens in the list
		// of objects, so apply the reduceOperator function in taking into account
		// the operator priorities
		in = reduceOperators (in, CompositionOperator.POWER);
		in = reduceOperators (in, CompositionOperator.MULTIPLY, CompositionOperator.DIVIDE  );
		in = reduceUnaryMinus(in);
		in = reduceOperators (in, CompositionOperator.ADD     , CompositionOperator.SUBTRACT);
		
		// Now there should only be one element in the list
		if(in.size()!=1) {
			throw new Expression.ParsingException("Badly formatted algebraic expression.");
		}
		Expression retVal = (Expression)in.get(0);
		if(retVal==null) {
			throw new Expression.ParsingException("Badly formatted algebraic expression.");
		}
		return retVal;
	}
	
	/**
	 * Reduce the expressions with parenthesis
	 */
	private static List<Object> reduceParenthesis(List<?> in)
	{
		// There are three possible states while going through the list of input objects 
		int state = 0;
		// - 0 : no opening parenthesis has been reached yet, or it has been closed
		// - 1 : the current object is inside a couple of parenthesis that isolates an expression
		// - 2 : the current object is inside a couple of parenthesis that bounds a list of function arguments
		
		// Temporary accumulators
		List<Expression>   arguments  = new LinkedList<Expression>();
		LinkedList<Object> expression = new LinkedList<Object>();
		int                level      = 0 ; // parenthesis level
		String             function   = ""; // function name
		
		// Loop over the list of input tokens 
		LinkedList<Object> retVal       = new LinkedList<Object>();
		ListIterator<?>    it           = in.listIterator();
		while(it.hasNext()) {
			Object     current     = it.next();
			Token.Type currentType = extractTokenType(current);
			
			// State 0: outside parenthesis
			if(state==0) {
				if(currentType==Token.Type.OPEN_P) {
					Object     previous     = retVal.peekLast();
					Token.Type previousType = extractTokenType(previous);
					if(previousType==Token.Type.ID) {
						retVal.pollLast();
						function = (String)((Token)previous).getValue();
						state    = 2;
					}
					else {
						state = 1;
					}
				}
				else {
					retVal.add(current);
				}
			}
			
			// State 1 or 2: inside an list of function arguments
			else {
				if(currentType==Token.Type.OPEN_P) {
					++level;
					expression.add(current);
				}
				else if(currentType==Token.Type.CLOSE_P) {
					if(level>0) {
						--level;
						expression.add(current);
					}
					else { // then level==0
						if(state==1) {
							retVal.add(interpretAsExpression(expression));
							expression.clear();
						}
						else { // then state==2
							arguments.add(interpretAsExpression(expression));
							expression.clear();
							retVal.add(new ExpressionFunction(function, arguments));
							arguments.clear();
						}
						state = 0;
					}
				}
				else if(currentType==Token.Type.COMMA && state==2 && level==0) {
					arguments.add(interpretAsExpression(expression));
					expression.clear();
				}
				else {
					expression.add(current);
				}
			}
		}
		
		// If state is not equal to 0 at the end, it means that there is a problem with parenthesis
		if(state!=0) {
			throw new Expression.ParsingException("Badly placed parenthesis.");
		}
		return retVal;
	}
	
	/**
	 * Reduce the given operators
	 */
	private static List<Object> reduceOperators(List<?> in, CompositionOperator ... operators)
	{
		// Construct a set containing all the token types that corresponds to an
		// operator that should be reduced at this stage
		Map<Token.Type, CompositionOperator> targets = new HashMap<Token.Type, CompositionOperator>();
		for(CompositionOperator o : operators) {
			targets.put(o.getToken(), o);
		}
		
		// Go through the list searching for operators to reduce
		LinkedList<Object> retVal  = new LinkedList<Object>();
		ListIterator<?>    it      = in.listIterator();
		while(it.hasNext()) {
			Object              current     = it.next();
			Token.Type          currentType = extractTokenType(current);
			CompositionOperator operator    = currentType==null ? null : targets.get(currentType);
			if(operator!=null) {
				if(retVal.isEmpty()) {
					throw new Expression.ParsingException("No expression on the left of operator " + currentType + ".");
				}
				if(!it.hasNext()) {
					throw new Expression.ParsingException("No expression on the right of operator " + currentType + ".");
				}
				Object lhs = retVal.pollLast();
				Object rhs = it.next();
				if(!(lhs instanceof Expression)) {
					throw new Expression.ParsingException("No expression on the left of operator " + currentType + ".");
				}
				if(!(rhs instanceof Expression)) {
					throw new Expression.ParsingException("No expression on the right of operator " + currentType + ".");
				}
				retVal.add(new ExpressionComposition(operator, (Expression)lhs, (Expression)rhs));
			}
			else {
				retVal.add(current);
			}
		}
		return retVal;
	}
	
	/**
	 * Look for a minus sign in front of the current list of objects
	 */
	private static List<Object> reduceUnaryMinus(List<?> in)
	{
		// Initialization
		LinkedList<Object> retVal  = new LinkedList<Object>();
		ListIterator<?>    it      = in.listIterator();
		
		// Look at the first (and maybe second) token
		if(!it.hasNext()) {
			return retVal;
		}
		Object first = it.next();
		if(extractTokenType(first)==Token.Type.MINUS) {
			if(!it.hasNext()) {
				throw new Expression.ParsingException("No expression on the right of operator - (unary).");
			}
			Object second = it.next();
			if(!(second instanceof Expression)) {
				throw new Expression.ParsingException("No expression on the right of operator - (unary).");
			}
			retVal.add(new ExpressionOpposite((Expression)second));
		}
		else {
			retVal.add(first);
		}
		
		// Copy the rest of the list
		while(it.hasNext()) {
			Object current = it.next();
			retVal.add(current);
		}
		return retVal;
	}
	
	/**
	 * Identify all the 'Constant'-kind and 'Variable'-kind expressions
	 */
	private static List<Object> identifyTerminalExpressions(List<?> in)
	{
		LinkedList<Object> retVal = new LinkedList<Object>();
		ListIterator<?>    it     = in.listIterator();
		while(it.hasNext()) {
			Object     current     = it.next();
			Token.Type currentType = extractTokenType(current);
			
			// Deal with constants
			if(currentType==Token.Type.LITTERAL) {
				double value = (Double)((Token)current).getValue();
				retVal.add(new ExpressionConstant(value));
			}
			
			// Deal with variable IDs
			else if(currentType==Token.Type.ID) {
				String variableName = (String)((Token)current).getValue();
				retVal.add(new ExpressionVariable(variableName));
			}
			
			// General case
			else {
				retVal.add(current);
			}
		}
		return retVal;
	}
	
	/**
	 * Return the token type of an object if it actually is Token, or null otherwise
	 */
	private static Token.Type extractTokenType(Object obj)
	{
		if(obj==null || !(obj instanceof Token)) {
			return null;
		}
		else {
			return ((Token)obj).getType();
		}
	}
}
