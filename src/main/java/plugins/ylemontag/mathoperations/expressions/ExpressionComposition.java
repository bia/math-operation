package plugins.ylemontag.mathoperations.expressions;

import java.util.SortedSet;

import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Node in a Math expression syntax tree that represents a composition between
 * two expressions through a composition operator
 */
public class ExpressionComposition extends Expression
{
	private CompositionOperator _operator;
	private Expression          _lhs     ;
	private Expression          _rhs     ;
	
	/**
	 * Constructor
	 */
	public ExpressionComposition(CompositionOperator operator, Expression lhs, Expression rhs)
	{
		super();
		_operator = operator;
		_lhs      = lhs     ;
		_rhs      = rhs     ;
	}
	
	/**
	 * Operator
	 */
	public CompositionOperator getOperator()
	{
		return _operator;
	}
	
	/**
	 * Left-hand side
	 */
	public Expression getLhs()
	{
		return _lhs;
	}
	
	/**
	 * Right-hand side
	 */
	public Expression getRhs()
	{
		return _rhs;
	}
	
	@Override
	public boolean equals(Expression expr)
	{
		if(!(expr instanceof ExpressionComposition)) {
			return false;
		}
		ExpressionComposition e = (ExpressionComposition)expr;
		return _operator==e._operator && _lhs.equals(e._lhs) && _rhs.equals(e._rhs);
	}
	
	@Override
	public String getRepresentation(boolean smartParenthesis)
	{
		String representationL = _lhs.getRepresentation(smartParenthesis);
		String representationR = _rhs.getRepresentation(smartParenthesis);
		representationL = addParenthesisIfNecessary(representationL, false, smartParenthesis);
		representationR = addParenthesisIfNecessary(representationR, true , smartParenthesis);
		return representationL + _operator.getToken().getName() + representationR;
	}
	
	/**
	 * Add parenthesis to the representation of the given expression if they are
	 * necessary to ensure operator priority consistency
	 */
	private String addParenthesisIfNecessary(String in, boolean rhs, boolean smartParenthesis)
	{
		Expression e      = rhs ? _rhs : _lhs;
		String     retVal = in;
		if(e instanceof ExpressionOpposite) {
			int myPriority   = _operator.getPriority();
			if(!smartParenthesis || myPriority>0 || (rhs && myPriority==0)) {
				retVal = "(" + retVal + ")";
			}
		}
		else if(e instanceof ExpressionComposition) {
			int myPriority   = _operator.getPriority();
			int exprPriority = ((ExpressionComposition)e).getOperator().getPriority();
			if(!smartParenthesis || exprPriority<myPriority || (rhs && exprPriority==myPriority)) {
				retVal = "(" + retVal + ")";
			}
		}
		return retVal;
	}
	
	@Override
	public SortedSet<String> getVariables()
	{
		SortedSet<String> retVal = _lhs.getVariables();
		retVal.addAll(_rhs.getVariables());
		return retVal;
	}
	
	@Override
	public boolean isValidFunctor(String[] variables)
	{
		return _lhs.isValidFunctor(variables) && _rhs.isValidFunctor(variables);
	}
	
	@Override
	public Functor getFunctor(String[] variables)
	{
		Functor functorL = _lhs.getFunctor(variables);
		Functor functorR = _rhs.getFunctor(variables);
		String patternL = addParenthesisIfNecessary(functorL.getFormatPattern(), false, true);
		String patternR = addParenthesisIfNecessary(functorR.getFormatPattern(), true , true);
		final Functor.FunPtr funPtrL = functorL.getFunctionPointer();
		final Functor.FunPtr funPtrR = functorR.getFunctionPointer();
		switch(_operator)
		{
			case ADD: return new Functor(variables.length, patternL + " + " + patternR, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return funPtrL.apply(in) + funPtrR.apply(in);
				}
			});
			
			case SUBTRACT: return new Functor(variables.length, patternL + " - " + patternR, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return funPtrL.apply(in) - funPtrR.apply(in);
				}
			});
			
			case MULTIPLY: return new Functor(variables.length, patternL + "\u00d7" + patternR, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return funPtrL.apply(in) * funPtrR.apply(in);
				}
			});
			
			case DIVIDE: return new Functor(variables.length, patternL + "\u00f7" + patternR, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return funPtrL.apply(in) / funPtrR.apply(in);
				}
			});
			
			case POWER: return new Functor(variables.length, patternL + "^" + patternR, new Functor.FunPtr()
			{
				@Override
				public double apply(double[] in)
				{
					return Math.pow(funPtrL.apply(in), funPtrR.apply(in));
				}
			});
			
			default:
				throw new RuntimeException("Unreachable code point");
		}
	}
}
