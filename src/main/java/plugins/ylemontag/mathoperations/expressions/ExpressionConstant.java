package plugins.ylemontag.mathoperations.expressions;

import icy.util.StringUtil;

import java.util.SortedSet;
import java.util.TreeSet;

import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Terminal node in an Math expression syntaxic tree, representing a constant value
 * 
 * End users should not use this class.
 */
public class ExpressionConstant extends Expression
{
	private double _value;
	
	/**
	 * Constructor
	 */
	public ExpressionConstant(double value)
	{
		super();
		_value = value;
	}
	
	/**
	 * Constant value
	 */
	public double getValue()
	{
		return _value;
	}
	
	@Override
	public boolean equals(Expression expr)
	{
		return (expr instanceof ExpressionConstant) && _value==((ExpressionConstant)expr)._value;
	}
	
	@Override
	public String getRepresentation(boolean smartParenthesis)
	{
		return StringUtil.toString(_value);
	}
	
	@Override
	public SortedSet<String> getVariables()
	{
		return new TreeSet<String>();
	}
	
	@Override
	public boolean isValidFunctor(String[] variables)
	{
		return true;
	}
	
	@Override
	public Functor getFunctor(String[] variables)
	{
		return new Functor(variables.length, StringUtil.toString(_value), new Functor.FunPtr()
		{
			@Override
			public double apply(double[] in)
			{
				return _value;
			}
		});
	}
}
