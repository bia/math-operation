package plugins.ylemontag.mathoperations;

import icy.sequence.Sequence;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * This class associate a sequence and a sub-range of T, Z and C indexes
 * 
 * A SubSequence object can be passed in argument to a functor object to execute
 * the assocated operation over a sub-part of a sequence. For instance, if 'seq'
 * is a sequence with 3 channels, then the following code will return a 
 * sequence with 1 channel, equal to the arithmetic mean of the 3 channels of
 * the sequence 'seq':
 * 
 * <code>
 * 
 *   SubSequence ch0 = new SubSequence(seq);
 *   SubSequence ch1 = new SubSequence(seq);
 *   SubSequence ch2 = new SubSequence(seq);
 *   
 *   ch0.setC(new int[] { 0 });
 *   ch1.setC(new int[] { 1 });
 *   ch2.setC(new int[] { 2 });
 *   
 *   return Functor3.parse("(a+b+c) / 3").apply(ch0, ch1, ch2);
 * 
 * </code>
 */
public class SubSequence
{
	/**
	 * Type of sub-range
	 */
	public static enum SubRangeType
	{
		INVALID  ,
		FULL     ,
		EMPTY    ,
		SINGLETON,
		INTERVAL ,
		OTHER    ;
	}
	
	private Sequence _sequence;
	private int[]    _t       ;
	private int[]    _z       ;
	private int[]    _c       ;
	
	@Override
	public String toString()
	{
		return _sequence.getName() + getSubRangeAsString(); 
	}
	
	/**
	 * Simple constructor (select all the available frames, slices, and channels in the given sequence)
	 * @throws IllegalArgumentException If sequence==null
	 */
	public SubSequence(Sequence sequence)
	{
		this(sequence, null, null, null);
	}
	
	/**
	 * Constructor
	 * @throws IllegalArgumentException If sequence==null
	 */
	public SubSequence(Sequence sequence, int[] t, int[] z, int[] c)
	{
		if(sequence==null) {
			throw new IllegalArgumentException("Illegal null sequence in sub-sequence constructor.");
		}
		_sequence = sequence;
		_t        = t       ;
		_z        = z       ;
		_c        = c       ;
	}
	
	/**
	 * Return the underlying sequence
	 */
	public Sequence getSequence()
	{
		return _sequence;
	}
	
	/**
	 * Return the sub-range of T indexes (a null result means "all the frames")
	 */
	public int[] getT()
	{
		return _t;
	}
	
	/**
	 * Return the sub-range of Z indexes (a null result means "all the slices")
	 */
	public int[] getZ()
	{
		return _z;
	}
	
	/**
	 * Return the sub-range of C indexes (a null result means "all the channels")
	 */
	public int[] getC()
	{
		return _c;
	}
	
	/**
	 * Set the sub-range of T indexes (pass null to select all the frames)
	 */
	public void setT(int[] t)
	{
		_t = t;
	}
	
	/**
	 * Set the sub-range of Z indexes (pass null to select all the slices)
	 */
	public void setZ(int[] z)
	{
		_z = z;
	}
	
	/**
	 * Set the sub-range of C indexes (pass null to select all the channels)
	 */
	public void setC(int[] c)
	{
		_c = c;
	}
	
	/**
	 * Size of the underlying sequence in the X dimension
	 */
	public int getSizeX()
	{
		return _sequence.getSizeX();
	}
	
	/**
	 * Size of the underlying sequence in the Y dimension
	 */
	public int getSizeY()
	{
		return _sequence.getSizeY();
	}
	
	/**
	 * Size of the selected sub-range in the Z dimension
	 */
	public int getSizeZ()
	{
		return _z==null ? _sequence.getSizeZ() : _z.length;
	}
	
	/**
	 * Size of the selected sub-range in the T dimension
	 */
	public int getSizeT()
	{
		return _t==null ? _sequence.getSizeT() : _t.length;
	}
	
	/**
	 * Size of the selected sub-range in the C dimension
	 */
	public int getSizeC()
	{
		return _c==null ? _sequence.getSizeC() : _c.length;
	}
	
	/**
	 * Is all the sequence selected?
	 */
	public boolean isFullySelected()
	{
		return
			getSubRangeTypeT()==SubRangeType.FULL &&
			getSubRangeTypeZ()==SubRangeType.FULL &&
			getSubRangeTypeC()==SubRangeType.FULL;
	}
	
	/**
	 * Type of selected sub-range in the T dimension
	 */
	public SubRangeType getSubRangeTypeT()
	{
		return getSubRangeType(_t, _sequence.getSizeT());
	}
	
	/**
	 * Type of selected sub-range in the Z dimension
	 */
	public SubRangeType getSubRangeTypeZ()
	{
		return getSubRangeType(_z, _sequence.getSizeZ());
	}
	
	/**
	 * Type of selected sub-range in the C dimension
	 */
	public SubRangeType getSubRangeTypeC()
	{
		return getSubRangeType(_c, _sequence.getSizeC());
	}
	
	/**
	 * Determine the type of sub-range
	 */
	public static SubRangeType getSubRangeType(int[] subRange, int sequenceSize)
	{
		// Special case: sub-range array either null or empty
		if(subRange==null) {
			return SubRangeType.FULL;
		}
		else if(subRange.length==0) {
			return sequenceSize==0 ? SubRangeType.FULL : SubRangeType.EMPTY;
		}
		
		// Regular case: sub-range length >=1
		else
		{	
			// Make sure that the first index is valid 
			int prevIdx = subRange[0];
			if(prevIdx<0 || prevIdx>=sequenceSize) {
				return SubRangeType.INVALID;
			}
			
			// Special case if the sub-range is a singleton
			if(subRange.length==1) {
				return sequenceSize==1 ? SubRangeType.FULL : SubRangeType.SINGLETON;
			}
			
			// Generic case: determine if the sub-range is an interval or not
			boolean admissibleForInterval = true;
			for(int k=1; k<subRange.length; ++k) {
				int currIdx = subRange[k];
				if(currIdx<0 || currIdx>=sequenceSize){
					return SubRangeType.INVALID;
				}
				if(currIdx!=prevIdx+1) {
					admissibleForInterval = false;
				}
				prevIdx = currIdx;
			}
			if(admissibleForInterval) {
				return (subRange[0]==0 && subRange.length==sequenceSize) ? SubRangeType.FULL : SubRangeType.INTERVAL;
			}
			else {
				return SubRangeType.OTHER;
			}
		}
	}
	
	/**
	 * Check if the selected sub-ranges are valid with respect to the dimensions
	 * of the underlying sequence
	 * 
	 * For instance, selecting c=[0 1 2] is not valid if the underlying sequence
	 * has only one (or two) channel(s).
	 */
	public boolean isValid()
	{
		return
			isSubRangeValid(_t, _sequence.getSizeT()) &&
			isSubRangeValid(_z, _sequence.getSizeZ()) &&
			isSubRangeValid(_c, _sequence.getSizeC());
	}
	
	/**
	 * Return true if the array is null or if its elements are within the range [0 ... sequenceSize-1]
	 */
	public static boolean isSubRangeValid(int[] subRange, int sequenceSize)
	{
		if(subRange==null) {
			return true;
		}
		for(int i : subRange) {
			if(i<0 || i>=sequenceSize) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Name of the sub-sequence
	 */
	public String getSubRangeAsString()
	{
		SubRangeType typeRangeT = getSubRangeTypeT();
		SubRangeType typeRangeZ = getSubRangeTypeZ();
		SubRangeType typeRangeC = getSubRangeTypeC();
		
		// Special case if one of the sub-range is not valid
		if(typeRangeT==SubRangeType.INVALID || typeRangeZ==SubRangeType.INVALID || typeRangeC==SubRangeType.INVALID) {
			return "[invalid]";
		}
		
		// Otherwise, concatenate the strings corresponding to the index ranges in T, Z and C
		String indexString = concatenateSubRangeStrings(
			getSubRangeAsString("t", typeRangeT, _t),
			getSubRangeAsString("z", typeRangeZ, _z),
			getSubRangeAsString("c", typeRangeC, _c)
		);
		
		// Final result
		return indexString==null ? "" : ("[" + indexString + "]");
	}
	
	/**
	 * Return a string representing the given sub-range (assuming that it is of the given type)
	 */
	private static String getSubRangeAsString(String dimName, SubRangeType type, int[] subRange)
	{
		switch(type)
		{
			case INVALID  : return null;
			case FULL     : return null;
			case EMPTY    : return "no " + dimName;
			case SINGLETON: return dimName + "=" + subRange[0];
			case INTERVAL : return dimName + " in " + subRange[0] + "..." + subRange[subRange.length-1]; 
			case OTHER    :
				String retVal = dimName + " in ";
				for(int k=0; k<subRange.length; ++k) {
					if(k>0) {
						retVal += ",";
					}
					retVal += subRange[k];
				}
				return retVal;
			
			default:
				throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Concatenate several sub-range strings
	 */
	private static String concatenateSubRangeStrings(String ... subRangeStrings)
	{
		String retVal = null;
		for(String s : subRangeStrings) {
			if(retVal==null) {
				retVal = s;
			}
			else if(s!=null) {
				retVal += " ; " + s;
			}
		}
		return retVal;
	}
}
