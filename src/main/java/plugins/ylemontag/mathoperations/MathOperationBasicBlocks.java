package plugins.ylemontag.mathoperations;

import plugins.adufour.blocks.util.VarList;
import plugins.ylemontag.mathoperations.operations.Operation2;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Simple blocks with operation pre-selection
 */
public class MathOperationBasicBlocks
{
	/**
	 * Generic block
	 */
	private static class AbstractBlock extends MathOperationAbstractBlock
	{
		private Operation2 _operation;
		private String     _name1    ;
		private String     _name2    ;
		
		protected AbstractBlock(Operation2 operation)
		{
			super(operation.describeOperation("a", "b"));
			_operation = operation;
			_name1     = "a"      ;
			_name2     = "b"      ;
		}
		
		@Override
		public void declareInput(VarList inputMap)
		{
			addInputVariant(inputMap, _name1, "In 1"); // The IDs are set as "In 1" and "In 2"
			addInputVariant(inputMap, _name2, "In 2"); // for compatibility reasons
		}
		
		@Override
		public String getMainPluginClassName()
		{
			return MathOperationPlugin.class.getName();
		}
		
		@Override
		public String getName()
		{
			return _operation.getFunctionName();
		}
		
		@Override
		public void run()
		{
			// Retrieve the input values
			Variant in1 = retrieveInputValue(_name1);
			Variant in2 = retrieveInputValue(_name2);
			String description = _operation.getFunctor().describeOperation(
				in1.getRepresentation(true), in2.getRepresentation(true));
			
			// Execute the operation and save the value
			try {
				Variant out = _operation.getFunctor().apply(in1, in2);
				defineOutputValue(out);
			}
			catch(Functor.InconsistentArguments err) {
				reportError(err, description);
			}
		}
	}
	
	/**
	 * Add block
	 */
	public static class AddBlock extends AbstractBlock
	{
		public AddBlock()
		{
			super(Operation2.ADD);
		}
	}
	
	/**
	 * Subtract block
	 */
	public static class SubtractBlock extends AbstractBlock
	{
		public SubtractBlock()
		{
			super(Operation2.SUBTRACT);
		}
	}
	
	/**
	 * Multiply block
	 */
	public static class MultiplyBlock extends AbstractBlock
	{
		public MultiplyBlock()
		{
			super(Operation2.MULTIPLY);
		}
	}
	
	/**
	 * Divide block
	 */
	public static class DivideBlock extends AbstractBlock
	{
		public DivideBlock()
		{
			super(Operation2.DIVIDE);
		}
	}
}
