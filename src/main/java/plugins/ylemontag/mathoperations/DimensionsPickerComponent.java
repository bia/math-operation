package plugins.ylemontag.mathoperations;

import icy.sequence.DimensionId;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component to select a sub-range of indexes
 */
public class DimensionsPickerComponent extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener interface
	 */
	public static interface ValueListener
	{
		/**
		 * Method triggered when the index range in the component changes
		 * @param valid boolean
		 * @param newValue array of int
		 */
		public void valueChanged(boolean valid, int[] newValue);
	}
	
	// Miscellaneous
	private DimensionId         _dimension     ;
	private List<ValueListener> _listeners     ;
	private boolean             _shuntListeners;
	
	// Radio buttons
	private JRadioButton _buttonAll      ;
	private JRadioButton _buttonSelection;
	private ButtonGroup  _buttonGroup    ;
	private boolean      _allSelected    ;
	
	// Text field
	private IntegerArrayTextField _indexField;
	
	/**
	 * Default constructor
	 */
	public DimensionsPickerComponent()
	{
		this(DimensionId.NULL);
	}
	
	/**
	 * Constructor
	 * @param dimension DimensionId
	 */
	public DimensionsPickerComponent(DimensionId dimension)
	{
		super(new BorderLayout(5, 5));
		_dimension      = dimension;
		_listeners      = new LinkedList<ValueListener>();
		_shuntListeners = false;
		
		// Radio buttons
		_allSelected     = true;
		_buttonAll       = new JRadioButton(makeButtonAllLabel(), true);
		_buttonSelection = new JRadioButton("Selection");
		_buttonGroup     = new ButtonGroup();
		_buttonGroup.add(_buttonAll      );
		_buttonGroup.add(_buttonSelection);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(_buttonAll);
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(_buttonSelection);
		buttonPanel.add(Box.createHorizontalGlue());
		add(buttonPanel, BorderLayout.NORTH);
		_buttonAll.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(_shuntListeners) {
					return;
				}
				onButtonClicked(true);
			}
		});
		_buttonSelection.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(_shuntListeners) {
					return;
				}
				onButtonClicked(false);
			}
		});
		
		// Index text field
		_indexField = new IntegerArrayTextField();
		add(_indexField, BorderLayout.CENTER);
		_indexField.setEnabled(false);
		_indexField.addValueListener(new IntegerArrayTextField.ValueListener<int[]>()
		{
			@Override
			public void valueChanged(int[] newValue)
			{
				if(_shuntListeners) {
					return;
				}
				onIndexFieldChanged();
			}
		});
	}
	
	/**
	 * Add a new listener
	 * @param l listener
	 */
	public void addValueListener(ValueListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 * @param l listener
	 */
	public void removeValueListener(ValueListener l)
	{
		_listeners.remove(l);
	}
	
	/**
	 * @return Is the content of the component consistent
	 */
	public boolean isValueValid()
	{
		return _allSelected || _indexField.getValue()!=null;
	}
	
	/**
	 * @return  the selected subset of indexes
	 */
	public int[] getValue()
	{
		return _allSelected ? null : _indexField.getValue();
	}
	
	/**
	 * Change the selected subset of indexes
	 * @param value array of int
	 */
	public void setValue(int[] value)
	{
		boolean wasValid = isValueValid();
		int[]   oldValue = getValue();
		_shuntListeners = true;
		if(value==null) {
			_allSelected = true;
			_buttonAll.setSelected(true);
			_indexField.setEnabled(false);
			if(_indexField.getValue()==null) {
				_indexField.setValue(new int[0]);
			}
		}
		else {
			_allSelected = false;
			_buttonSelection.setSelected(true);
			_indexField.setEnabled(true);
			_indexField.setValue(value);
		}
		_shuntListeners = false;
		if(!wasValid || !areIdenticIntegerArray(oldValue, value)) {
			fireValueListeners();
		}
	}
	
	/**
	 * Action performed when one of the radio button is clicked
	 */
	private void onButtonClicked(boolean allButtonClicked)
	{
		if(allButtonClicked==_allSelected) {
			return;
		}
		_indexField.setEnabled(!allButtonClicked);
		_allSelected = allButtonClicked;
		fireValueListeners();
	}
	
	/**
	 * Action performed when the value in the text field is changed
	 */
	private void onIndexFieldChanged()
	{
		if(_allSelected) {
			return;
		}
		fireValueListeners();
	}
	
	/**
	 * Fire the value listeners
	 */
	private void fireValueListeners()
	{
		boolean valid    = isValueValid();
		int[]   newValue = getValue();
		for(ValueListener l : _listeners) {
			l.valueChanged(valid, newValue);
		}
	}
	
	/**
	 * Label of the "All" radio button
	 */
	private String makeButtonAllLabel()
	{
		if(_dimension==null || _dimension==DimensionId.NULL) {
			return "All";
		}
		else {
			switch(_dimension) {
				case X: return "All columns" ;
				case Y: return "All rows"    ;
				case Z: return "All slices"  ;
				case T: return "All frames"  ;
				case C: return "All channels";
				default:
					throw new RuntimeException("Unreachable code point");
			}
		}
	}
	
	/**
	 * Default text in the index field
	 */
	private String makeNoSelectionText()
	{
		if(_dimension==null || _dimension==DimensionId.NULL) {
			return "Nothing";
		}
		else {
			switch(_dimension) {
				case X: return "No column" ;
				case Y: return "No row"    ;
				case Z: return "No slice"  ;
				case T: return "No frame"  ;
				case C: return "No channel";
				default:
					throw new RuntimeException("Unreachable code point");
			}
		}
	}
	
	/**
	 * Check if both arrays are the same
	 */
	private static boolean areIdenticIntegerArray(int[] obj1, int[] obj2)
	{
		// Special case for null values
		if(obj1==null || obj2==null) {
			return obj1==obj2;
		}
		
		// Check the length
		int lg = obj1.length;
		if(obj2.length!=lg) {
			return false;
		}
		
		// Check the content
		for(int k=0; k<lg; ++k) {
			if(obj1[k]!=obj2[k]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Implement the abstract class FormattedTextField to parse list of integers
	 */
	private class IntegerArrayTextField extends FormattedTextField<int[]>
	{
		private static final long serialVersionUID = 1L;

		/**
		 * Constructor
		 */
		public IntegerArrayTextField()
		{
			super();
			initializeComponent();
		}
		
		@Override
		protected String makeHelpText()
		{
			return makeNoSelectionText();
		}
		
		@Override
		protected int[] stringToObject(String text)
		{
			try {
				text = text.trim();
				if(text.isEmpty()) {
					return new int[0];
				}
				String[] rawValues = text.split(" +");
				int[] retVal = new int[rawValues.length];
				for(int k=0; k<retVal.length; ++k) {
					retVal[k] = Integer.parseInt(rawValues[k]);
				}
				return retVal;
			}
			catch(NumberFormatException err) {
				return null;
			}
		}
		
		@Override
		protected String objectToString(int[] object)
		{
			if(object==null) {
				return "[invalid]";
			}
			String retVal = "";
			for(int k=0; k<object.length; ++k) {
				if(k>0) {
					retVal += " ";
				}
				retVal += object[k];
			}
			return retVal;
		}
		
		@Override
		protected boolean areIdenticObjects(int[] obj1, int[] obj2)
		{
			return areIdenticIntegerArray(obj1, obj2);
		}
	}
}
