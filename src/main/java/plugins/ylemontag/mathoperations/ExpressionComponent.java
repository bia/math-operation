package plugins.ylemontag.mathoperations;

import icy.gui.frame.GenericFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component to edit and display an expression
 */
public class ExpressionComponent extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener interface
	 */
	public static interface ValueListener
	{
		/**
		 * Method triggered when the expression in the component changes
		 * @param newValue Expression
		 */
		public void valueChanged(Expression newValue);
	}
	
	private List<ValueListener> _listeners     ;
	private String[]            _validVariables;
	private Expression          _value         ;
	private boolean             _isTextEmpty   ;
	private JTextField          _textField     ;
	private JButton             _helpButton    ;
	
	/**
	 * Default constructor
	 */
	public ExpressionComponent()
	{
		this(null);
	}
	
	/**
	 * Constructor
	 * @param validVariables array of String
	 */
	public ExpressionComponent(String[] validVariables)
	{
		// Initialization
		super(new BorderLayout(5, 5));
		_listeners      = new LinkedList<ValueListener>();
		_validVariables = validVariables;
		_value          = null;
		_isTextEmpty    = true;
		
		// Text field
		_textField = new JTextField();
		add(_textField, BorderLayout.CENTER);
		_textField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onComponentValueChanged(_textField.getText());
			}
		});
		_textField.addFocusListener(new FocusAdapter()
		{
			@Override
			public void focusLost(FocusEvent e)
			{
				onComponentValueChanged(_textField.getText());
			}

			@Override
			public void focusGained(FocusEvent e)
			{
				if(_isTextEmpty) {
					_textField.setText("");
					_textField.setForeground(Color.BLACK);
				}
			}
		});
		updateComponentText("");
		
		// Help button
		_helpButton = new JButton("?");
		Dimension dim = _helpButton.getPreferredSize();
		dim.setSize(30, dim.getHeight());
		_helpButton.setMinimumSize  (dim);
		_helpButton.setPreferredSize(dim);
		add(_helpButton, BorderLayout.EAST);
		_helpButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				showHelpFrame();
			}
		});
	}
	
	/**
	 * Add a new listener
	 * @param l Listener
	 */
	public void addValueListener(ValueListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 * @param l Listener
	 */
	public void removeValueListener(ValueListener l)
	{
		_listeners.remove(l);
	}
	
	/**
	 * @return Retrieve the expression
	 */
	public Expression getValue()
	{
		return _value;
	}
	
	/**
	 * Set the expression displayed in the component
	 * @param value Expression
	 */
	public void setValue(Expression value)
	{
		Expression oldValue = _value;
		if(isExpressionValid(value)) {
			_value = value;
			updateComponentText(value.getRepresentation(true));
		}
		else {
			_value = null;
			updateComponentText("");
		}
		if(isSameExpression(oldValue)) {
			return;
		}
		fireValueListeners();
	}
	
	/**
	 * Action performed when the component lost the focus or the user hit the <Enter> key
	 */
	private void onComponentValueChanged(String newText)
	{
		Expression oldValue = _value;
		_value = getValidatedExpression(newText);
		updateComponentText(_value==null ? newText : _value.getRepresentation(true));
		if(isSameExpression(oldValue)) {
			return;
		}
		fireValueListeners();
	}
	
	/**
	 * Set the text in the text field
	 */
	private void updateComponentText(String text)
	{
		if(text.equals("")) {
			_isTextEmpty = true;
			if(_textField.hasFocus()) {
				_textField.setText("");
				_textField.setForeground(Color.BLACK);
			}
			else {
				_textField.setText("Example: x+log(y)");
				_textField.setForeground(Color.GRAY);
			}
		}
		else {
			_isTextEmpty = false;
			_textField.setText(text);
			_textField.setForeground(_value==null ? Color.RED : Color.BLACK);
		}
	}
	
	/**
	 * Return the validated expression the corresponds to the given text, or
	 * null if this text does not represents a valid expression 
	 */
	private Expression getValidatedExpression(String text)
	{
		try {
			Expression expr = Expression.parse(text);
			if(isExpressionValid(expr)) {
				return expr;
			}
			else {
				return null;
			}
		}
		catch(Expression.ParsingException err) {
			return null;
		}
	}
	
	/**
	 * Check whether the given expression is valid or not
	 */
	private boolean isExpressionValid(Expression expression)
	{
		if(expression==null) {
			return false;
		}
		else {
			return expression.isValidFunctor(_validVariables);
		}
	}
	
	/**
	 * Check if the new value is equal to the current one
	 */
	private boolean isSameExpression(Expression newValue)
	{
		if(_value==null) {
			return newValue==null;
		}
		else {
			return _value.equals(newValue);
		}
	}
	
	/**
	 * Fire the value listeners
	 */
	private void fireValueListeners()
	{
		for(ValueListener l : _listeners) {
			l.valueChanged(_value);
		}
	}
	
	/**
	 * Display the help frame
	 */
	private void showHelpFrame()
	{
		String    title   = "Syntax for mathematical expressions";
		JTextPane message = new JTextPane();
		message.setEditable(false);
		message.setContentType("text/html");
		message.setText(
			"<h3>Examples</h3>" +
			"<ul>" +
				"<li><tt>5*a + log(3/b) - c^2</tt></li>" +
				"<li><tt>-x + max(y, abs(z))</tt></li>" +
				"<li><tt>rho*cos(theta) + alpha^(-2)</tt></li>" +
			"</ul>" +
			"<h3>Available functions</h3>" +
			"<p>All the following functions expect one argument, unless otherwise specified.</p>" +
			"<ul>" +
				"<li><tt>abs</tt>&nbsp;: absolute value</li>" +
				"<li><tt>sign</tt>&nbsp;: sign extraction</li>" +
				"<li><tt>sqrt</tt>&nbsp;: square root</li>" +
				"<li><tt>exp</tt>&nbsp;: exponential function</li>" +
				"<li><tt>log</tt>&nbsp;: natural logarithm</li>" +
				"<li><tt>log10</tt>&nbsp;: decimal logarithm</li>" +
				"<li><tt>cos</tt>&nbsp;: cosine</li>" +
				"<li><tt>sin</tt>&nbsp;: sine</li>" +
				"<li><tt>tan</tt>&nbsp;: tangent</li>" +
				"<li><tt>acos</tt>&nbsp;: arc cosine</li>" +
				"<li><tt>asin</tt>&nbsp;: arc sine</li>" +
				"<li><tt>atan</tt>&nbsp;: arc tangent</li>" +
				"<li><tt>cosh</tt>&nbsp;: hyperbolic cosine</li>" +
				"<li><tt>sinh</tt>&nbsp;: hyperbolic sine</li>" +
				"<li><tt>tanh</tt>&nbsp;: hyperbolic tangent</li>" +
				"<li><tt>round</tt>&nbsp;: rounding to the closest integer</li>" +
				"<li><tt>floor</tt>&nbsp;: rounding to the largest previous integer</li>" +
				"<li><tt>ceil</tt>&nbsp;: rounding to the smallest following integer</li>" +
				"<li><tt>max</tt>&nbsp;: maximum (expects two arguments)</li>" +
				"<li><tt>min</tt>&nbsp;: minimum (expects two arguments)</li>" +
			"</ul>" +
			"<h3>Available operators</h3>" +
			"<ul>" +
				"<li><tt>+</tt>&nbsp;: addition</li>" +
				"<li><tt>-</tt>&nbsp;: substraction</li>" +
				"<li><tt>*</tt>&nbsp;: multiplication</li>" +
				"<li><tt>/</tt>&nbsp;: division</li>" +
				"<li><tt>^</tt>&nbsp;: power</li>" +
			"</ul>" +
			"<p>" +
				"All the operators are left-associative, which means that <tt>a/b/c</tt> " +
				"will be evaluated as <tt>(a/b)/c</tt>. Usual precedence rules between operators apply: " +
				"operator <tt>^</tt> is evaluated first, then <tt>*</tt> and <tt>/</tt>, " +
				"and finally <tt>+</tt> and <tt>-</tt>. For instance, <tt>-a^3 + 4/3*b</tt> " +
				"will be interpreted as <tt>(-(a^3)) + ((4/3)*b)</tt>." +
			"</p>"
		);
		Dimension dim = message.getPreferredSize();
		dim.setSize(600, dim.getHeight()+100);
		message.setPreferredSize(dim);
		JScrollPane scroll = new JScrollPane(message);
		dim = scroll.getPreferredSize();
		dim.setSize(600, 500);
		scroll.setPreferredSize(dim);
		GenericFrame infoFrame = new GenericFrame(title, scroll);
		infoFrame.addToMainDesktopPane();
		infoFrame.setVisible(true);
		infoFrame.requestFocus();
	}
}
