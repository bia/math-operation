package plugins.ylemontag.mathoperations;

import icy.sequence.Sequence;
import plugins.ylemontag.mathoperations.variants.VariantArray;
import plugins.ylemontag.mathoperations.variants.VariantScalar;
import plugins.ylemontag.mathoperations.variants.VariantSequence;
import plugins.ylemontag.mathoperations.variants.VariantSubSequence;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap objects of different types to be used as input/output of a Functor apply method
 * 
 * When an object (such as a Sequence, a double array, etc...) is wrapped as a
 * Variant, it is seen as a long buffer of double values, whose length is given
 * by the method 'getSize()'. This buffer can be browsed using iterators returned
 * by the 'getReadIterator()' and 'getWriteIterator()' methods.
 */
public abstract class Variant
{
	/**
	 * Underlying types of returned objects
	 */
	public static enum DimensionType
	{
		SCALAR  ("Scalar"  ),
		ARRAY   ("Array"   ),
		SEQUENCE("Sequence");
		
		private String _name;
		
		private DimensionType(String name)
		{
			_name = name;
		}
		
		@Override
		public String toString()
		{
			return _name;
		}
	}
	
	
	/**
	 * Available types of wrappable objects
	 */
	public static enum Type
	{
		SCALAR     ("Scalar"      , DimensionType.SCALAR  ),
		ARRAY      ("Array"       , DimensionType.ARRAY   ),
		SEQUENCE   ("Sequence"    , DimensionType.SEQUENCE),
		SUBSEQUENCE("Sub-sequence", DimensionType.SEQUENCE);
		
		private String        _name         ;
		private DimensionType _dimensionType;
		
		private Type(String name, DimensionType dimensionType)
		{
			_name          = name         ;
			_dimensionType = dimensionType;
		}
		
		public DimensionType getDimensionType()
		{
			return _dimensionType;
		}
		
		@Override
		public String toString()
		{
			return _name;
		}
	}
	
	
	/**
	 * Dimensions that characterize a wrapped object
	 */
	public static abstract class Dimension
	{
		private DimensionType _type;
		
		@Override
		public boolean equals(Object object)
		{
			if(object==null || !(object instanceof Dimension)) {
				return false;
			}
			else {
				return equals((Dimension)object);
			}
		}
		
		@Override
		public String toString()
		{
			return getRepresentation();
		}
		
		/**
		 * Constructor
		 */
		protected Dimension(DimensionType type)
		{
			_type = type;
		}
		
		/**
		 * Type of the underlying object
		 */
		public DimensionType getType()
		{
			return _type;
		}
		
		/**
		 * Function in charge of checking that both sizes are equal
		 */
		public abstract boolean equals(Dimension dimension);
		
		/**
		 * Return a human-readable representation of the size and the type
		 */
		public abstract String getRepresentation();
		
		/**
		 * Return the length of the underlying buffer
		 */
		public abstract long getFlatSize();
		
		/**
		 * Granularity
		 * 
		 * In a multi-thread context, the buffer is supposed to be browse an process
		 * by several threads. If the granularity is set to 2 or more, then the
		 * transitions between threads must be aligned on blocks of length granularity.
		 * The granularity is always a divisor of the flat size.
		 */
		public abstract int getGranularity();
		
		/**
		 * Allocate a new variant object of the corresponding type and size
		 */
		public abstract Variant allocateNewVariant();
	}
	
	
	/**
	 * Base interface for iterators
	 */
	public interface Iterator
	{
		/**
		 * Return true if the end of the buffer has not been reached yet
		 */
		public boolean valid();
		
		/**
		 * Go to the next sample
		 */
		public void next();
		
		/**
		 * Skip a certain number of samples at the beginning of the buffer and initialize the iterator
		 */
		public void startAt(long num);
	}
	
	
	/**
	 * Read iterator
	 */
	public interface ReadIterator extends Iterator
	{
		/**
		 * Return the value of the current sample
		 * @warning Undefined behavior if !valid()
		 */
		public double get();
	}
	
	
	/**
	 * Write iterator
	 */
	public interface WriteIterator extends Iterator
	{
		/**
		 * Set the value of the current sample
		 * @warning Undefined behavior if !valid()
		 */
		public void set(double value);
	}
	
	
	private Type    _type    ;
	private boolean _updating;
	
	/**
	 * Constructor
	 */
	protected Variant(Type type)
	{
		_type     = type ;
		_updating = false;
	}
	
	/**
	 * Return the underlying type
	 */
	public Type getType()
	{
		return _type;
	}
	
	/**
	 * Return the value casted as a double
	 * @throws IllegalStateException If getType!=SCALAR
	 */
	public double getAsScalar()
	{
		throw new IllegalStateException("This variant object is not a scalar.");
	}
	
	/**
	 * Return the value casted as an array
	 * @throws IllegalStateException If getType!=ARRAY
	 */
	public double[] getAsArray()
	{
		throw new IllegalStateException("This variant object is not an array.");
	}
	
	/**
	 * Return the value casted as a sequence
	 * @throws IllegalStateException If getType!=SEQUENCE
	 */
	public Sequence getAsSequence()
	{
		throw new IllegalStateException("This variant object is not a sequence.");
	}
	
	/**
	 * Return the value casted as a sub-sequence
	 * @throws IllegalStateException If getType!=SUBSEQUENCE
	 */
	public SubSequence getAsSubSequence()
	{
		throw new IllegalStateException("This variant object is not a sub-sequence.");
	}
	
	/**
	 * Return a string representing the underlying object
	 * 
	 * If addParenthesisIfFragile is set to true, and if the string representing
	 * the object contains spaces, or other special characters that may be part of
	 * a math expression, then the returned string is surrounded with parenthesis.
	 */
	public abstract String getRepresentation(boolean addParenthesisIfFragile);
	
	@Override
	public String toString()
	{
		return getRepresentation(false);
	}
	
	/**
	 * Determine if a string should be considered as fragile or not
	 * 
	 * A string should be considered as fragile if, when substituted to a variable
	 * name within a math expression, it compromises the understanding of this
	 * math expression.
	 * 
	 * Example: consider the math expression "2*a" and the string "image1 + image2"
	 * If this string is substitued to the variable 'a', the math expression
	 * becomes "2*image1 + image2", which is not what we could expect as a
	 * description of the underlying operation: better would be "2*(image1 + image2)".
	 * Therefore, the string "image1 + image2" must be considered as fragile.
	 */
	public static boolean isFragileString(String s)
	{
		int lg = s.length();
		for(int k=0; k<lg; ++k) {
			switch(s.charAt(k)) {
				case ' ':
				case '^':
				case '\u00d7':
				case '\u00f7':
					return true;
				default:
					break;
			}
		}
		return false;
	}
	
	/**
	 * Return the object that characterize the dimensions of the underlying object
	 */
	public abstract Dimension getDimension();
	
	/**
	 * Is the object readable?
	 * 
	 * For instance, dynamically allocated sequences are not readable.
	 */
	public abstract boolean isReadable();
	
	/**
	 * Is the object writable?
	 * 
	 * For instance, non-double-valued sequences are not writable.
	 */
	public abstract boolean isWritable();
	
	/**
	 * Allocate a new iterator to browse the buffer in read mode
	 * @throws IllegalStateException If !isReadable()
	 */
	public ReadIterator getReadIterator()
	{
		if(!isReadable()) {
			throw new IllegalStateException("The current object is not readable.");
		}
		return implGetReadIterator();
	}
	
	/**
	 * Implement getReadIterator()
	 */
	protected abstract ReadIterator implGetReadIterator();
	
	/**
	 * Allocate a new iterator to browse the buffer in write mode
	 * @throws IllegalStateException If beginUpdate() has not be called yet.
	 */
	public WriteIterator getWriteIterator()
	{
		if(!_updating) {
			throw new IllegalStateException(
				"Must call beginUpdate() before creating a iterator in write mode."
			);
		}
		return implGetWriteIterator();
	}
	
	/**
	 * Implement getWriteIterator()
	 */
	protected abstract WriteIterator implGetWriteIterator();
	
	/**
	 * This method must be called before allocating a write-iterator
	 * @throws IllegalStateException If !isWritable()
	 */
	public void beginUpdate()
	{
		if(!isWritable()) {
			throw new IllegalStateException("The current object is not writable.");
		}
		implBeginUpdate();
		_updating = true;
	}
	
	/**
	 * Implement beginUpdate()
	 */
	protected abstract void implBeginUpdate();
	
	/**
	 * This method must be called after using write-iterators
	 */
	public void endUpdate()
	{
		_updating = false;
		implEndUpdate();
	}
	
	/**
	 * Implement endUpdate()
	 */
	protected abstract void implEndUpdate();
	
	/**
	 * Wrap a scalar as a Variant
	 */
	public static Variant wrap(double value)
	{
		return new VariantScalar(value);
	}
	
	/**
	 * Wrap an existing array as a Variant
	 */
	public static Variant wrap(double[] value)
	{
		return new VariantArray(value);
	}
	
	/**
	 * Wrap an existing sequence as a Variant
	 */
	public static Variant wrap(Sequence value)
	{
		return new VariantSequence(value);
	}
	
	/**
	 * Wrap an existing sub-sequence as a Variant
	 */
	public static Variant wrap(SubSequence value)
	{
		return new VariantSubSequence(value);
	}
}
