package plugins.ylemontag.mathoperations;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Basic math operation
 * 
 * This interface represents a basic math operation, such as addition,
 * logarithm, maximum, etc. Each operation correspond to a math function, that
 * is represented by a Functor object. Examples:
 * 
 *    Operation |      Functor
 *   -------------------------------
 *    Addition  | (a,b) -&gt; a + b
 *    Logarithm | a -&gt; log(a)
 *    Maximum   | (a,b) -&gt; max(a,b)
 *  
 *  However, the contrary is not true: for example, the math function
 *  (a, b, c) -&gt; a + log(b*cos(c)) / 2 does not correspond to any basic
 *  operation (it does even have a name!).
 *  
 *  The available basic operations are listed in the Operation1 and Operation2
 *  enumerations, depending on their number of input arguments.
 */
public interface Operation
{
	/**
	 * Full name of the operation
	 */
	public String getFunctionName();
	
	/**
	 * Raw functor associated to the operation
	 * 
	 * The derived classes Operation1 and Operation2 provide a method getFunctor(),
	 * which also returns the functor associated the operatior, but casted
	 * respectively as Functor1 or Functor2 objects. The classes Functor1 and 
	 * Functor2 provide more convenient apply() methods than the base class Functor,
	 * therefore final users may prefer use the method getFunctor().
	 */
	public Functor getRawFunctor();
}
