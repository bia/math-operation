package plugins.ylemontag.mathoperations;

import icy.system.IcyHandledException;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.vars.gui.model.VarEditorModel;
import plugins.ylemontag.mathoperations.operations.Operation1;
import plugins.ylemontag.mathoperations.operations.Operation2;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Perform the basic math operations
 */
public class MathOperationPlugin extends MathOperationAbstractPlugin
{
	/**
	 * Wrap VarOperation into the EzVar structure
	 */
	private static class EzVarOperation extends EzVar<Operation>
	{
		/**
		 * Constructor
		 */
		public EzVarOperation(String name, Operation defaultValue)
		{
			super(new VarOperation(name, defaultValue), new VarEditorModel<Operation>()
			{
				@Override
				public boolean isValid(Operation value) {
					return true;
				}

				@Override
				public Operation getDefaultValue() {
					return Operation2.ADD;
				}
			});
		}
	}
	
	private EzVar<Operation> _operation = new EzVarOperation("Operation", Operation2.ADD);
	private EzGroupVariant _group0 = new EzGroupVariant("Input"  );
	private EzGroupVariant _group1 = new EzGroupVariant("Input 1");
	private EzGroupVariant _group2 = new EzGroupVariant("Input 2");
	
	/**
	 * Refresh the visibility of all the input fields
	 */
	private void refreshVisibility()
	{
		boolean unaryOp  = (_operation.getValue() instanceof Operation1);
		boolean binaryOp = (_operation.getValue() instanceof Operation2);
		_group0.setVisible(unaryOp );
		_group1.setVisible(binaryOp);
		_group2.setVisible(binaryOp);
	}
	
	@Override
	protected void initialize()
	{
		addEzComponent(_operation);
		addEzComponent(_group0   );
		addEzComponent(_group1   );
		addEzComponent(_group2   );
		_operation.addVarChangeListener(new EzVarListener<Operation>()
		{
			@Override
			public void variableChanged(EzVar<Operation> source, Operation newValue) {
				refreshVisibility();
			}
		});
		refreshVisibility();
	}
	
	@Override
	protected void execute()
	{
		// Initialization
		Operation op  = _operation.getValue();
		Functor   fun = op.getRawFunctor();
		
		// Binary operations
		if(op instanceof Operation2) {
			coreExecute(fun, _group1, _group2);
		}
		
		// Unary operations
		else if(op instanceof Operation1) {
			coreExecute(fun, _group0);
		}
		
		// Other cases
		else {
			throw new IcyHandledException("No operation selected");
		}
	}
}
